<?php

// Define defaults
define('ENVIRONMENT', 'test');

// Define constants for DB
define('DB_DRIVER', 'mysql');
define('DB_HOST', 'example');
define('DB_NAME', 'example');
define('DB_PORT', '3306');
define('DB_USERNAME', 'example');
define('DB_PASSWORD', 'example');

// Define cached file
define('TOKEN_CACHED_FILE', 'example');

// Define constants for customer
define('DEV_DOMAIN', 'example');
define('STAGING_DOMAIN', 'example');
define('LIVE_DOMAIN', 'example');
define('MOVE_DOMAIN', 'example');

define('DEV_DOMAIN_ACHIEVE_SERVICE', 'example');
define('STAGING_DOMAIN_ACHIEVE_SERVICE', 'example');
define('LIVE_DOMAIN_ACHIEVE_SERVICE', 'example');
define('MOVE_DOMAIN_ACHIEVESERVICE', 'example');

define('DB_HOSTNAME_DEV', 'example_hostname_dev');
define('DB_HOSTNAME_STAGING', 'example_hostname_staging');
define('DB_HOSTNAME_LIVE', 'example_hostname_live');

// Define Default mail constraints
define('DEFAULT_MAIL_SERVER', 'smtp.firmstep.com');
define('DEFAULT_MAIL_PORT', 25);
define('DEFAULT_MAIL_FROM', 'Firmstep Platform <platform@firmstep.com>');

// Define Health Check mail constants (disabled)
define('HEALTH_CHECK_MAIL_SERVER', ''); // smtp.firmstep.com
define('HEALTH_CHECK_MAIL_PORT', 25);
define('HEALTH_CHECK_MAIL_FROM', 'Firmstep Platform <platform@firmstep.com>');
define('HEALTH_CHECK_MAIL_TO', 'Auckland Team <auckland-team@firmstep.com>');
define('HEALTH_CHECK_MAIL_SUBJECT', 'Tunnel Health Check [test]');

// Stages allowed for customers
define('STAGES_ALLOWED', [ 'DEV', 'STAGING', 'LIVE' ]);

// Github token
define('GIT_TOKEN', 'git_token_example');

// Define Slack settings
define('SLACK_URL', 'example_url');
define('SLACK_CHANNEL', 'example_channel');
define('SLACK_USERNAME', 'example_username');

// Define AWS SDK user
define('AWS_KEY', 'example_aws_key');
define('AWS_SECRET', 'example_aws_secret');
define('AWS_REGION', 'example_aws_region');
define('AWS_BACKUP_EFS_BUCKET', 'example_aws_bucket');
define('AWS_BACKUP_DB_BUCKET', 'example_aws_bucket');
