<?php

require __DIR__ . '/../Autoload.php';

$configItems = ['common'];
$configPath  = __DIR__ . '/../config/';

foreach ($configItems as $configItem) {
    $configFile = $configPath . $configItem . '.php';

    if (is_readable($configFile)) {
        require $configFile;
    } else {
        die('Error: Configure failed for {' . $configItem . '}, check configure file ' . $configItem . '.php in config folder');
    }
}
