<?php

ini_set('max_execution_time', 180);

// BOOTSTRAP
require __DIR__ . '/bootstrap.php';

// ROUTER
$router = new FS\Common\Router();

// For Tunnel API
$router->post('/api/tunnel/domain', function () {
    return (new FS\API\Tunnel\Handler\Tunnel())->getDomainByIP();
});

// For CMS Server API
$router->group('/api/cms/', function () {
    $this->post('config', function () { return (new FS\API\CMS\Handler\CMS())->getApache2Config(); });
    $this->post('rebuild', function () { return (new FS\API\CMS\Handler\CMS())->getBash(); });
});

// CRUD APIs for table `customer`
$router->group('/api/customer/', function () {
    $this->post('create', function () { return (new FS\API\CMSMaster\Handler\Customer())->create(); });
    $this->post('list', function () { return (new FS\API\CMSMaster\Handler\Customer())->getList(); });
    $this->post('get/(\w+)', function ($id) { return (new FS\API\CMSMaster\Handler\Customer())->getById($id); });
    $this->post('update', function () { return (new FS\API\CMSMaster\Handler\Customer())->update(); });
    $this->post('delete/(\w+)', function ($id) { return (new FS\API\CMSMaster\Handler\Customer())->delete($id); });
});

// For Queue API
$router->post('/api/queue/create', function () {
    return (new FS\API\Queue\Handler\Queue())->create();
});

$router->execute();
