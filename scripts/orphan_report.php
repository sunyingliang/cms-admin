<?php

require __DIR__ . '/../config/common.php';
require __DIR__ . '/../classes/Common/IO.php';

use FS\Common\IO;

try {
    $pdo = new \PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USERNAME, DB_PASSWORD);

    if (!$pdo) {
        IO::doMessage('Unable to establish MySQL connection, check connection settings');
    }

    // Check if there are customers need to clean
    $customers = $pdo->query("SELECT concat(`dev_database`, ',', `staging_database`, ',', `live_database`) AS `database`, `directory` FROM `customer`")->fetchAll();

    $slackMessage        = '';
    $orphanUser          = [];
    $orphanDB            = [];
    $orphanEFSDir        = [];
    $customerDbString    = '';
    $customerDirectories = [];

    foreach ($customers as $customer) {
        $customerDbString .= $customer['database'] . ',';
        array_push($customerDirectories, $customer['directory']);
    }

    $cmsMasterDbs = array_filter(explode(',', $customerDbString));

    $databases = $pdo->query("SELECT TABLE_SCHEMA AS `database` FROM information_schema.TABLES WHERE TABLE_NAME = 'search_dataset' AND TABLE_SCHEMA NOT LIKE '%rrc_drupal%';")->fetchAll();

    // If customer database does not in solutions_master, push it into orphan database array
    foreach ($databases as $database) {
        if (!in_array($database['database'], $cmsMasterDbs)) {
            array_push($orphanDB, $database['database']);

            $stmt = $pdo->prepare("SELECT Db, User FROM mysql.db WHERE Db = :db OR Db = :db_slash");
            $stmt->execute([
                'db'       => $database['database'],
                'db_slash' => str_replace('_', '\_', $database['database'])
            ]);

            $users = $stmt->fetchAll();

            // Check if the database user used by other database
            foreach ($users as $user) {
                $stmt = $pdo->prepare("SELECT COUNT(1) AS `total` FROM mysql.db WHERE `User` =:dbUser");
                $stmt->execute(['dbUser' => $user['User']]);
                $count = $stmt->fetch();

                if ($count['total'] == 1) {
                    array_push($orphanUser, $user['User']);
                }
            }
        }
    }

    // Check EFS backup directories, if the directory in /efs not in cms_master, report it
    $allDirs = [];
    if (file_exists('/efs')) {
        chdir('/efs');

        $dirs = ['CMS-dev', 'CMS-live', 'CMS-staging'];

        foreach ($dirs as $dir) {
            if (file_exists('/efs/' . $dir)) {
                chdir('/efs/' . $dir);
                $subDirs = array_filter(glob('*'), 'is_dir');
                $allDirs = array_unique(array_merge_recursive($allDirs, $subDirs));
            }
        }

        $orphanEFSDir = array_diff($allDirs, $customerDirectories);
    }

    // Send slack message
    if (empty($orphanUser) && empty($orphanDB) && empty($orphanEFSDir)) {
        $slackMessage = "CMS orphan check complete, no cleanup required";
    } else {
        $slackMessage = 'CMS orphan check complete, the following need addressing on ' . DB_HOST . PHP_EOL . PHP_EOL;
        $slackMessage .= !empty($orphanDB) ? "Database: " . implode(", ", $orphanDB) . PHP_EOL : '';
        $slackMessage .= !empty($orphanUser) ? "Database User: " . implode(", ", $orphanUser) . PHP_EOL : '';
        $slackMessage .= !empty($orphanEFSDir) ? "EFS Directory: " . implode(", ", $orphanEFSDir) . PHP_EOL : '';
    }

    IO::doMessage($slackMessage, true);
} catch (Exception $ex) {
    IO::domessage($ex->getMessage());
}
