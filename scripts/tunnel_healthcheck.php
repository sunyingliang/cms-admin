<?php

require __DIR__ . '/../config/common.php';
require __DIR__ . '/../classes/Common/Mail.php';

// Database config settings
$config       = [];
$config['db'] = [
    'host'     => DB_HOST,
    'name'     => DB_NAME,
    'username' => DB_USERNAME,
    'password' => DB_PASSWORD
];

// Check if disabled
if (empty(HEALTH_CHECK_MAIL_SERVER)) {
    exit(PHP_EOL . 'Tunnel Health Check disabled.' . PHP_EOL);
}

//Mail config settings
$mailConfig = [
    'server'  => HEALTH_CHECK_MAIL_SERVER,
    'port'    => HEALTH_CHECK_MAIL_PORT,
    'from'    => HEALTH_CHECK_MAIL_FROM,
    'to'      => HEALTH_CHECK_MAIL_TO,
    'subject' => HEALTH_CHECK_MAIL_SUBJECT
];

// Check connection is working before proceeding (DNS might not be fully working on reboot)
$pdo     = null;
$counter = 0;

while ($counter < 10) {
    try {
        if (!is_array($config)) {
            die('Error: Configuration file is not set properly');
        }

        $pdo = new \PDO('mysql:host=' . $config['db']['host'] . ';dbname=' . $config['db']['name'],
            $config['db']['username'], $config['db']['password']);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

        $counter = 10;
    } catch (Exception $e) {
        $pdo = null;
        sleep(30);
        $counter++;
    }
}

if ($pdo === null) {
    die('Error: Cannot connect to database "' . $config['db']['name'] . '" on "' . $config['db']['host'] . '" server');
}

$sql         = "SELECT `name`, `live_tunnel_endpoint`, `live_tunnel_ip` FROM `customer` WHERE `enabled` = 1";
$stmt        = $pdo->query($sql);
$objCustomer = $stmt->fetchAll();
$list        = [];

foreach ($objCustomer as $customer) {
    if (isset($customer['live_tunnel_ip']) && !empty($customer['live_tunnel_ip'])) {

        // Initialise CURL Request
        $ip = $customer['live_tunnel_ip'];
        $ch = curl_init('http://' . $ip);

        // Set CURL Options
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        // Get CURL Response
        $response = curl_exec($ch);

        // Close CURL Request
        curl_close($ch);

        // Finally check if the response is empty, if it is, add it to the list
        if (empty($response)) {
            $list[] = 'Name: ' . $customer['name'] . ', Endpoint: ' . $customer['live_tunnel_endpoint'];
        }
    }
}

if (!empty($list)) {
    try {
        $mailer  = new Mail($mailConfig);
        $message = 'The following CMS HTTP/HTTPS Tunnels are either not responding or their endpoint is not accessible: ' . PHP_EOL . PHP_EOL . implode(' ' . PHP_EOL, $list);
        $mailer->setMessage($message);
        $mailer->send();
        echo('HealthCheck report complete.' . PHP_EOL);
    } catch (Exception $e) {
        echo('Error: ' . $e->getMessage());
        echo $e;
    }
}

return true;
