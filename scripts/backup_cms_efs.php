<?php

// Info: This script is designed to backup CMS EFS to S3 periodically

// Require CLI
if (php_sapi_name() != 'cli') {
    die('Error: This script can only be run from command line only, exiting...' . PHP_EOL);  
} 

// Initialise prerequisites
require __DIR__ . '/../aws-sdk/aws-autoloader.php';
require __DIR__ . '/../Autoload.php';
require __DIR__ . '/../config/common.php';

// Initialise BackupCMSEFS class
$backupCMSEFS = new \FS\Backup\BackupCMSEFS();

// Process Customer EFS backup
$backupCMSEFS->process();
