<?php

if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../Autoload.php';
require __DIR__ . '/../config/common.php';

$queueProcessor = new \FS\CronJob\QueueProcessor();
$queueProcessor->process();
