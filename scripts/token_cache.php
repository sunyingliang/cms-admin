<?php

// Check server api
if (php_sapi_name() != 'cli') {
    die('Must be run via cli');
}

require __DIR__ . '/../Autoload.php';
require __DIR__ . '/../config/common.php';

// Config settings
$config       = [];
$config['db'] = [
    'host'     => DB_HOST,
    'name'     => DB_NAME,
    'username' => DB_USERNAME,
    'password' => DB_PASSWORD
];

// Check connection is working before proceeding (DNS might not be fully working on @reboot)
$pdo     = null;
$counter = 0;

while ($counter < 10) {
    try {
        if (!is_array($config)) {
            die('Error: Configuration file is not set properly');
        }

        $pdo = new \PDO('mysql:host=' . $config['db']['host'] . ';dbname=' . $config['db']['name'], $config['db']['username'], $config['db']['password']);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

        $counter = 10;
    } catch (Exception $e) {
        $pdo = null;
        sleep(30);
        $counter++;
    }
}

if ($pdo === null) {
    die('Error: Cannot connect to database "' . $config['db']['name'] . '" on "' . $config['db']['host'] . '" server');
}

try {
    $cachedTokens = '';

    $sql = 'SELECT `name`, `key`, `read`, `write` FROM `token`';

    if (($stmt = $pdo->query($sql)) === false) {
        throw new \FS\Common\Exception\PDOQueryException('PDO query failed');
    }

    while ($row = $stmt->fetch()) {
        $cachedTokens .= $row['name'] . '|' . $row['key'] . '|' . $row['read'] . '|' . $row['write'] . ';';
    }

    $cachedTokens = substr($cachedTokens, 0, -1);

    $wroteChars = file_put_contents(TOKEN_CACHED_FILE, $cachedTokens, LOCK_EX);

    if ($wroteChars != strlen($cachedTokens)) {
        throw new \FS\Common\Exception\FileWriteException('Failed to cache tokens');
    }
} catch (\Exception $e) {
    \FS\Common\IO::errorMessage($e->getMessage(), true);
}

\FS\Common\IO::errorMessage('Tokens have been cached successfully.' . PHP_EOL, true);
