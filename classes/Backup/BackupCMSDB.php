<?php

namespace FS\Backup;

use FS\API\CMSAdminBase;
use FS\Common\Exception\DBDuplicationException;
use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\FileExistException;
use FS\Common\Exception\DirCreationException;
use FS\Common\Exception\FileWriteException;
use FS\Common\Database;
use FS\Common\IO;
use Aws\S3\S3Client;
use Aws\S3\MultipartUploader;

class BackupCMSDB extends CMSAdminBase
{
    public static $testEnvironment = 'TEST';
    public static $testRuntime     = '12';
    public static $defaultRuntime  = '00';

    public function process()
    {
        $hasStarted = false;

        try {
            IO::doMessage('Creating directory');

            $dbDir = $this->createDirectory();

            IO::doMessage('Searching for customers');

            $sql = 'SELECT * FROM `customer`';

            if (($stmt = $this->db->prepare($sql)) === false) {
                throw new PDOCreationException('PDOStatement prepare failed');
            }

            if ($stmt->execute() === false) {
                throw new PDOCreationException('PDOStatement execution failed');
            }

            $customers = $stmt->fetchAll();

            IO::doMessage('Processing customers');

            foreach ($customers as $customer) {
                if ($customer['enabled'] == 1) {

                    // We're only making a backup of LIVE customer data, so only process customer if it is live status
                    if (strtolower($customer['stage']) == 'live') {
                        IO::doMessage('Customer ' . $customer['name'] . ' skipped, only customers at LIVE status have granular backups');
                        continue;
                    }

                    // If no live database detail, skip but add note in log (no more exception)
                    if (empty($customer['live_hostname']) || empty($customer['live_database']) || empty($customer['live_username']) || empty($customer['live_password'])) {
                        IO::doMessage('Customer ' . $customer['name'] . ' skipped, missing LIVE database detail? Please check master record');
                        continue;
                    }

                    try {
                        $db = new Database([
                            'DB_HOST'     => $customer['live_hostname'],
                            'DB_PORT'     => 3306,
                            'DB_NAME'     => $customer['live_database'],
                            'DB_USERNAME' => $customer['live_username'],
                            'DB_PASSWORD' => $customer['live_password']
                        ]);

                        $db->test();
                    } catch (InvalidParameterException $ex) {
                        IO::doMessage('Customer ' . $customer['name'] . ': ' . $ex->getMessage());
                        continue;
                    } catch (PDOCreationException $ex) {
                        IO::doMessage('Customer ' . $customer['name'] . ': ' . $ex->getMessage());
                        continue;
                    }

                    // Check if the database should be backed up
                    $backupTime = $this->checkBackupFrequency($customer['backup_frequency']);

                    if (in_array(date("H"), $backupTime) || $customer['backup_frequency'] == 'Hourly') {

                        // Run message only at 12 a.m. or if we're in self::$testEnvironment at self::$testRuntime to help reduce spam
                        if (!$hasStarted && (date("H") == self::$defaultRuntime || (ENVIRONMENT == self::$testEnvironment && date("H") == self::$testRuntime))) {
                            IO::doMessage('CMS-Admin - Started DB backup [' . ENVIRONMENT . ']', true);
                            $hasStarted = true;
                        }
                        
                        $this->backupDatabase($db, $dbDir);
                    }
                }
            }

            // Check if database backup exists
            if (count(glob($dbDir . '/*')) > 0) {
                $this->backupToS3($dbDir);
            } else {
                IO::doMessage('No database needs to be backed up');
            }

            // Remove temporary folder
            IO::shell("rm -Rf {$dbDir}");

            if ($hasStarted) {
                IO::doMessage('CMS-Admin - Finished DB backup [' . ENVIRONMENT . ']', true);
            }

            exit('Script complete.' . PHP_EOL);
        } catch (\Exception $e) {
            IO::doMessage('CMS-Admin - An error occured when backing up CMS Customer DB to S3 [' . ENVIRONMENT . ']: ' . $e->getMessage(), true);

            die();
        }
    }

    private function checkBackupFrequency($backupFrequency)
    {
        $executeTime = [];

        switch ($backupFrequency) {
            case 'Daily':
                // Note: Set to self::$testRuntime for self::$testEnvironment as the server is not running at midnight
                $executeTime = (ENVIRONMENT == self::$testEnvironment) ? [self::$testRuntime] : ['00'];
                break;
            case '12 Hourly':
                $executeTime = ['00', '12'];
                break;
            case '6 Hourly':
                $executeTime = ['00', '06', '12', '18'];
                break;
        }

        return $executeTime;
    }

    private function backupDatabase($db, $dbDir)
    {
        $mysqldump_location      = $dbDir . '/' . $db->host . '_' . $db->name . '.sql';
        $mysqldump_conf_location = $dbDir . '/' . $db->host . '_' . $db->name . '.conf';

        // Clear out anything in case we're re-processing
        if (file_exists($mysqldump_location)) {
            unlink($mysqldump_location);
        }

        if (file_exists($mysqldump_conf_location)) {
            unlink($mysqldump_conf_location);  
        } 

        // Write FROM conf file, we're doing this to avoid showing user/pass in ps/history for security
        $mysqldump_conf_file = fopen($mysqldump_conf_location, "w");

        fwrite($mysqldump_conf_file, '[client]' . PHP_EOL);
        fwrite($mysqldump_conf_file, 'host = ' . $db->host . PHP_EOL);
        fwrite($mysqldump_conf_file, 'user = ' . $db->username . PHP_EOL);
        fwrite($mysqldump_conf_file, 'password = ' . $db->password . PHP_EOL);
        fclose($mysqldump_conf_file);

        IO::doMessage('Database ' . $db->name . ' config file generated');

        // Create the command
        $mysqldump_command = 'mysqldump --defaults-extra-file="' . $mysqldump_conf_location . '" ' . $db->name . ' --no-create-db > ' . $mysqldump_location;

        // Execute the command
        $response = IO::shell($mysqldump_command);

        if (!empty($response)) {
            throw new DBDuplicationException('Error backing up database ' . $db->name . ': ' . $response);
        }

        IO::doMessage('Finished database ' . $db->name . ' backup');

        unlink($mysqldump_conf_location);

        // Close the customer DB connections
        $db->close();
    }

    private function createDirectory()
    {
        $dbDir = getcwd() . '/customerDBBackup';

        if (file_exists($dbDir)) {

            // Remove the TO directory if it already exists
            $response = IO::shell("rm -Rf {$dbDir}");

            if (!empty($response)) {
                throw new FileWriteException('Failed to execute command {rm -Rf ' . $dbDir . '}: ' . $response);
            }
        }

        if (mkdir($dbDir, 0775, true) === false) {
            throw new DirCreationException('Could not create directory {' . $dbDir . '}');
        }

        return $dbDir;
    }

    private function backupToS3($dbDir)
    {
        IO::doMessage('Loading AWS S3Client');

        if (!defined('AWS_REGION') || !defined('AWS_KEY') || !defined('AWS_SECRET') || !defined('AWS_BACKUP_DB_BUCKET')) {
            throw new InvalidParameterException('Configuration for {AWS_REGION}, {AWS_KEY}, {AWS_SECRET} or {AWS_BACKUP_DB_BUCKET} are not set correctly in config file');
        }

        $s3Client = new S3Client([
            'region'      => AWS_REGION,
            'version'     => 'latest',
            'http'        => ['decode_content' => false],
            'credentials' => [
                'key'    => AWS_KEY,
                'secret' => AWS_SECRET
            ]
        ]);

        $fileName = 'cms_db_backups_' . date("YmdHi") . '.tz';

        IO::doMessage('Checking if backup exists before continuing');

        // Check if there already is a backup for today
        if ($s3Client->doesObjectExist(AWS_BACKUP_DB_BUCKET, $fileName) === true) {
            throw new FileExistException('Backup already exists');
        }

        IO::doMessage('Starting CMS DB compression');

        IO::shell('tar -zcvf ' . $fileName . ' -C ' . $dbDir . ' .');

        IO::doMessage('Finished CMS DB compression');

        if (!file_exists($fileName)) {
            throw new FileExistException('Backup could not be found');
        }

        IO::doMessage('Starting CMS DB to S3 upload');

        $attempts    = 0;
        $maxAttempts = 50;
        $uploader    = new MultipartUploader($s3Client, $fileName, [
            'bucket' => AWS_BACKUP_EFS_BUCKET,
            'key'    => $fileName
        ]);

        do {
            try {
                $result = $uploader->upload();
                IO::doMessage('Finished CMS EFS to S3 upload');
                break;
            } catch (\Aws\Exception\MultipartUploadException $e) {
                if ($attempts > $maxAttempts) {
                    IO::doMessage('Upload failed, max attempts exceeded');
                    $params = $e->getState()->getId();
                    $result = $s3Client->abortMultipartUpload($params);
                    break;
                }

                $uploader = new MultipartUploader($s3Client, $fileName, [
                    'state' => $e->getState()
                ]);

                IO::doMessage('Upload failed, attempt [' . $attempts . '/' . $maxAttempts .']:' . $e->getMessage());

                $attempts++;
                sleep(1);
            } catch (\Exception $e) {
                IO::doMessage('Error when uploading mutipart file to S3: ' . $e->getMessage());
                break;
            }
        } while (!isset($result));

        IO::doMessage('Removing local backup');
        
        unlink($fileName);
    }
}
