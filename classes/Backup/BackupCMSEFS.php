<?php

namespace FS\Backup;

use FS\API\CMSAdminBase;
use FS\Common\Exception\DBDuplicationException;
use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\FileExistException;
use FS\Common\Exception\DirCreationException;
use FS\Common\Exception\FileWriteException;
use FS\Common\Database;
use FS\Common\IO;
use Aws\S3\S3Client;
use Aws\S3\MultipartUploader;

class BackupCMSEFS
{
    public function process()
    {
        try {
            IO::doMessage('CMS-Admin - Started EFS backup [' . ENVIRONMENT . ']', true);

            // Check /efs mounted
            if (!file_exists('/efs')) {
                IO::doMessage('Re-mounting /efs as it is not mounted?');
                IO::shell('mount /efs');
            }

            IO::doMessage('Loading AWS S3Client');

            $s3Client = new S3Client([
                'region'  => AWS_REGION,
                'version' => 'latest',
                'http'    => ['decode_content' => false],
                'credentials' => [
                    'key'    => AWS_KEY,
                    'secret' => AWS_SECRET
                ]
            ]);

            IO::doMessage('Loaded AWS S3Client');

            // Define the filename, daily granularity
            $fileName = 'cms_efs_backup_' . date("Ymd") . '.tz';

            IO::doMessage('Checking if backup exists before continuing');

            // Check if there already is a backup for today
            if ($s3Client->doesObjectExist(AWS_BACKUP_EFS_BUCKET, $fileName) === true) {
               throw new \Exception('Backup already exists'); 
            }

            IO::doMessage('Backup does not already exist');

            IO::doMessage('Starting CMS EFS compression'); 

            IO::shell('tar -zcvf ' . $fileName . ' /efs');

            IO::doMessage('Finished CMS EFS compression');

            if (!file_exists($fileName)) {
                throw new \Exception('Backup could not be found'); 
            }

            IO::doMessage('Starting CMS EFS to S3 upload');
            
            $attempts    = 0;
            $maxAttempts = 50;
            $uploader    = new MultipartUploader($s3Client, $fileName, [
                'bucket' => AWS_BACKUP_EFS_BUCKET,
                'key'    => $fileName
            ]);

            do {
                try {
                    $result = $uploader->upload();
                    IO::doMessage('Finished CMS EFS to S3 upload');
                    break;
                } catch (\Aws\Exception\MultipartUploadException $e) {
                    if ($attempts > $maxAttempts) {
                        IO::doMessage('Upload failed, max attempts exceeded');
                        $params = $e->getState()->getId();
                        $result = $s3Client->abortMultipartUpload($params);
                        break;
                    }

                    $uploader = new MultipartUploader($s3Client, $fileName, [
                        'state' => $e->getState()
                    ]);

                    IO::doMessage('Upload failed, attempt [' . $attempts . '/' . $maxAttempts .']:' . $e->getMessage());

                    $attempts++;
                    sleep(1);
                } catch (\Exception $e) {
                    IO::doMessage('Error when uploading mutipart file to S3: ' . $e->getMessage());
                    break;
                }
            } while (!isset($result));

            IO::doMessage('Removing local backup');

            unlink($fileName);

            IO::doMessage('CMS-Admin - Finished EFS backup [' . ENVIRONMENT . ']', true);

            exit('Script complete.' . PHP_EOL);
        } catch (\Exception $e) {
            IO::doMessage('CMS-Admin - An error occured when backing up CMS EFS to S3 [' . ENVIRONMENT . ']: ' . $e->getMessage(), true);

            die();
        }
    }
}
