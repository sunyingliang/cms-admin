<?php

namespace FS\CronJob;

use FS\API\CMSAdminBase;
use FS\Common\Exception\CURLException;
use FS\Common\Exception\DBDuplicationException;
use FS\Common\Exception\DirCreationException;
use FS\Common\Exception\FileExistException;
use FS\Common\Exception\FileNotReadableException;
use FS\Common\Exception\FileWriteException;
use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\InvalidRepositoryException;
use FS\Common\Exception\MailException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\Exception\PDOResultEmptyException;
use FS\Common\Exception\ShellCommandException;
use FS\Common\Database;
use FS\Common\IO;
use FS\Common\Mail;

class QueueProcessor extends CMSAdminBase
{
    private $efsPrefix  = '/efs/CMS-';
    private $efsWebDir  = 'web';
    private $efsFileDir = 'files';
    private $efsRetry   = 3;
    private $efsDelay   = 5;
    private $gitToken   = '';

    public function __construct()
    {
        parent::__construct();

        $this->gitToken = GIT_TOKEN;
    }

    // PURPOSE: Process one item from the Queue database, frequency will be determined by the server cron job.
    public function process()
    {
        // Timeout 1 hour max
        set_time_limit(60 * 60);

        // Check /efs mounted
        if (!file_exists('/efs')) {
            IO::doMessage('Re-mounting /efs as it is not mounted?');
            IO::shell('mount /efs');
        }

        try {
            $sql = "SELECT `id`, `data` 
                    FROM `queue` 
                    WHERE `status` = 'PENDING' 
                    ORDER BY `created` ASC 
                    LIMIT 1";

            if (($stmt = $this->db->query($sql)) === false) {
                throw new PDOExecutionException('Unable to establish connection to Queue database');
            }

            if (($schedule = $stmt->fetch()) === false) {
                exit('Notice: No records to process' . PHP_EOL);
            }

            try {
                // Update item to PROCESSING
                $sql  = "UPDATE `queue` SET `status` = 'PROCESSING', `started` = CURRENT_TIMESTAMP, `finished` = null
                         WHERE `id` = :id";

                if (($stmt = $this->db->prepare($sql)) === false) {
                    throw new PDOCreationException('PDOStatement prepare failed when fetching customer detail');
                }

                if ($stmt->execute(['id' => $schedule['id']]) === false) {
                    throw new PDOExecutionException('PDOStatement execution failed');
                }

                // Do requested work
                $data = json_decode($schedule['data']);

                if (json_last_error() !== JSON_ERROR_NONE) {
                    throw new PDOResultEmptyException('Could not decode data');
                }

                if (!($validation = IO::required($data, ['function', 'id']))['valid']) {
                    throw new InvalidParameterException($validation['message']);
                }

                $customer  = $this->getCustomer($data->id);
                $directory = empty($customer['directory']) ? $customer['name'] : $customer['directory'];

                switch ($data->function) {
                    case 'promote-dev-to-staging':
                        $stageFrom = 'dev';
                        $stageTo   = 'staging';
                        $this->buildEFSStructure($directory, $stageTo);
                        $this->processDatabaseContent($customer, $stageFrom, $stageTo);
                        $this->processFileContent($directory, $stageFrom, $stageTo);
                        $this->processWebContent($customer, $directory, $stageTo);
                        break;
                    case 'promote-staging-to-live':
                        $stageFrom = 'staging';
                        $stageTo   = 'live';
                        $this->buildEFSStructure($directory, $stageTo);
                        $this->processDatabaseContent($customer, $stageFrom, $stageTo);
                        $this->processFileContent($directory, $stageFrom, $stageTo);
                        break;
                    case 'reset-dev-code':
                        $this->processWebContent($customer, $directory, 'dev');
                        break;
                    case 'reset-dev-data':
                        $stageFrom = 'live';
                        $stageTo   = 'dev';
                        $this->processDatabaseContent($customer, $stageFrom, $stageTo);
                        $this->processFileContent($directory, $stageFrom, $stageTo);
                        break;
                    case 'reset-staging-code':
                        $this->processWebContent($customer, $directory, 'staging');
                        break;
                    case 'reset-staging-data':
                        $stageFrom = 'live';
                        $stageTo   = 'staging';
                        $this->processDatabaseContent($customer, $stageFrom, $stageTo);
                        $this->processFileContent($directory, $stageFrom, $stageTo);
                        break;
                    default:
                        throw new InvalidParameterException('Function not recognised in queue data');
                        break;
                }

                // Adjust customer processing (optional stage) field when process is finished
                $sql = "UPDATE `customer` SET `processing` = 0";

                if ($data->function == 'promote-dev-to-staging') {
                    $stage = 'STAGING';
                } elseif ($data->function == 'promote-staging-to-live') {
                    $stage = 'LIVE';
                }

                if (!empty($stage)) {
                    $sql .= ", `stage` = '{$stage}'";
                }

                $sql .= " WHERE `id` = {$data->id}";

                if (!$this->db->exec($sql) && !$this->db->errorCode()) {
                    throw new PDOExecutionException('Failed to update processing field when process is finished');
                }

                // Update item to PROCESSED / ERROR
                $sql  = "UPDATE queue SET `status` = 'PROCESSED', `finished` = CURRENT_TIMESTAMP 
                         WHERE `id` = :id";

                if (($stmt = $this->db->prepare($sql)) === false) {
                    throw new PDOCreationException('PDOStatement prepare failed when fetching customer detail');
                }

                if ($stmt->execute(['id' => $schedule['id']]) === false) {
                    throw new PDOExecutionException('PDOStatement execution failed');
                }

                // Fetch Job start/finish detail & add to
                $sql = "SELECT `started`, `finished` 
                        FROM `queue` 
                        WHERE `id` = :id
                        LIMIT 1";

                if (($stmt = $this->db->prepare($sql)) === false) {
                    throw new PDOCreationException('PDOStatement prepare failed when fetching customer detail');
                }

                if ($stmt->execute(['id' => $schedule['id']]) === false) {
                    throw new PDOExecutionException('PDOStatement execution failed');
                }

                if (($queueItem = $stmt->fetch()) === false) {
                    exit('Notice: No records to process' . PHP_EOL);
                }

                $message = PHP_EOL . 'Started: ' . $queueItem['started'] . 
                           PHP_EOL . 'Finished: ' . $queueItem['finished'];

                $this->sendEmail($data, 'Status: PROCESSED' . PHP_EOL . PHP_EOL . print_r($data, true) . PHP_EOL . 'Customer: ' . $customer['name'] . PHP_EOL . $message);

                exit('Notice: Success' . PHP_EOL);
            } catch (\Exception $e) {
                
                $data = json_decode($schedule['data']);

                $sql = "UPDATE `customer` SET `processing` = 0 
                        WHERE `id` = :id";

                if (($stmt = $this->db->prepare($sql)) === false) {
                    throw new PDOCreationException('PDOStatement prepare failed when fetching customer detail');
                }

                if ($stmt->execute(['id' => $data->id]) === false) {
                    throw new PDOExecutionException('Failed to update processing field back when process is terminated with error');
                }

                $sql = "UPDATE `queue` SET `status` = 'ERROR', `finished` = CURRENT_TIMESTAMP, `message` = :message 
                        WHERE `id` = :id";

                $stmt    = $this->db->prepare($sql);
                $message = $e->getMessage();

                if ($stmt->execute(['message' => $e->getMessage(), 'id' => $schedule['id']]) === false) {
                    $message .= PHP_EOL . PHP_EOL . 'PDOStatement execution failed when updating queue record to error status...';
                }

                $this->sendEmail($data, 'Status: ERROR' . PHP_EOL . PHP_EOL . print_r($data, true) . PHP_EOL . PHP_EOL . 'Customer: ' . $customer['name'] . PHP_EOL . 'Message: ' . $message);

                die('Error: ' . $e->getMessage() . PHP_EOL);
            }
        } catch (\Exception $e) {
            die('Error: ' . $e->getMessage() . PHP_EOL);
        }
    }

    #region Utils
    private function buildEFSStructure($directory, $environment)
    {
        $customerEFSDir  = $this->efsPrefix . $environment . '/' . $directory;
        $customerWebDir  = $customerEFSDir . '/' . $this->efsWebDir;
        $customerFileDir = $customerEFSDir . '/' . $this->efsFileDir;

        if (file_exists($customerEFSDir)) {
            // Remove the TO directory if it already exists
            $response = IO::shell("rm -rf {$customerEFSDir}");

            if (!empty($response)) {
                throw new FileWriteException('Failed to execute command {rm -rf ' . $customerEFSDir . '}: ' . $response);
            }
        }

        if (mkdir($customerEFSDir, 0775, true) === false) {
            throw new DirCreationException('Could not create directory {' . $customerEFSDir . '}');
        }

        // Create customer /web (webroot) directory
        if ($environment !== 'live') {
            if (mkdir($customerWebDir) === false) {
                throw new DirCreationException('Could not create directory {' . $customerWebDir . '}');
            }
        }

        // Create customer /files (user-uploads) directory
        if (mkdir($customerFileDir) === false) {
            throw new DirCreationException('Could not create directory {' . $customerFileDir . '}');
        }

        // Change owner and modes
        $this->changeOwnerMode($customerEFSDir, 'www-data', 'www-data', '0775');
    }

    private function processWebContent($customer, $directory, $environment)
    {
        $efsWebDir   = $this->efsPrefix . $environment . '/' . $directory . '/' . $this->efsWebDir;

        // Download code from github
        $gitLocation = 'https://api.github.com/repos' . $customer['repository'] . '/tarball/' . $customer[$environment . '_branch'];
        $tempFile    = '/tmp/' . $customer['name'] . '.tar';

        // if temp file exists, delete it
        if (file_exists($tempFile)) {
            unlink($tempFile);
        }

        $response = IO::shell('curl -s -H "Authorization: token ' . $this->gitToken . '" -o ' . $tempFile . ' -L ' . $gitLocation);

        if (!empty($response)) {
            throw new CURLException('Failed to download code from git {' . $gitLocation . '}: ' . $response);
        }

        // Check response for 'Not Found'
        $fileContent = file_get_contents($tempFile);

        if (strpos($fileContent, 'Not Found')) {
            throw new InvalidRepositoryException('Git repository not found. Note: Repository name is case-sensitive and the CMS-Admin user needs access.');
        }

        // Move code to appropriate directory
        $response = IO::shell('rm -rf ' . $efsWebDir . '/*');

        if (!empty($response)) {
            throw new ShellCommandException('Failed to execute command {rm -rf ' . $efsWebDir . '/*}: ' . $response);
        }

        $response = IO::shell('tar xf ' . $tempFile . ' -C /tmp');

        if (!empty($response)) {
            throw new ShellCommandException('Failed to execute command {tar xf ' . $tempFile . ' -C /tmp}: ' . $response);
        }

        $outFolder = exec('ls -1c /tmp | grep ' . str_replace('/', '-', substr($customer['repository'], 1)) . '- | head -1');

        if (empty($outFolder)) {
            throw new ShellCommandException('Failed to find the downloaded directory');
        }

        $response = IO::shell('mv /tmp/' . $outFolder . '/* ' . $efsWebDir . '/');

        if (!empty($response)) {
            throw new ShellCommandException('Failed to execute command {mv /tmp/' . $outFolder . '/* ' . $efsWebDir . '/}: ' . $response);
        }

        $response = IO::shell('rm -f ' . $tempFile);

        if (!empty($response)) {
            throw new ShellCommandException('Failed to execute command {rm -f ' . $tempFile . '}: ' . $response);
        }

        $response = IO::shell('rm -rf /tmp/' . $outFolder);
        
        if (!empty($response)) {
            throw new ShellCommandException('Failed to execute command {rm -rf /tmp/' . $outFolder . '}: ' . $response);
        }

        // Change ownership and mode
        $this->changeOwnerMode($efsWebDir, 'www-data', 'www-data', '0775');
    }

    private function processDatabaseContent($customer, $databaseFrom, $databaseTo)
    {
        // Part 1: Validate and test FROM database
        $dbFrom = $this->validateCustomerDB($customer, $databaseFrom);

        // Part 2: Validate and test TO database
        $dbTo = $this->validateCustomerDB($customer, $databaseTo);

        // Part 3: Download FROM database
        $databaseFrom                 = strtoupper($databaseFrom);
        $databaseTo                   = strtoupper($databaseTo);
        $mysqldump_from_location      = getcwd() . '/' . $dbFrom->host . '_' . $dbFrom->name . '.sql';
        $mysqldump_from_conf_location = getcwd() . '/' . $dbFrom->host . '_' . $dbFrom->name . '.conf';
        $mysqldump_to_location        = getcwd() . '/' . $dbTo->host . '_' . $dbTo->name . '.sql';
        $mysql_to_conf_location       = getcwd() . '/' . $dbTo->host . '_' . $dbTo->name . '.conf';

        // Clear out anything in case we're re-processing
        if (file_exists($mysqldump_from_location)) unlink($mysqldump_from_location);
        if (file_exists($mysqldump_from_conf_location)) unlink($mysqldump_from_conf_location);
        if (file_exists($mysqldump_to_location)) unlink($mysqldump_to_location);
        if (file_exists($mysql_to_conf_location)) unlink($mysql_to_conf_location);

        // Write FROM conf file, we're doing this to avoid showing user/pass in ps/history for security
        $mysqldump_from_conf_file = fopen($mysqldump_from_conf_location, "w");

        fwrite($mysqldump_from_conf_file, '[client]' . PHP_EOL);
        fwrite($mysqldump_from_conf_file, 'host = ' . $dbFrom->host . PHP_EOL);
        fwrite($mysqldump_from_conf_file, 'user = ' . $dbFrom->username . PHP_EOL);
        fwrite($mysqldump_from_conf_file, 'password = ' . $dbFrom->password . PHP_EOL);
        fclose($mysqldump_from_conf_file);

        // Write TO conf file, we're doing this to avoid showing user/pass in ps/history for security
        $mysql_to_conf_file = fopen($mysql_to_conf_location, "w");
        fwrite($mysql_to_conf_file, '[client]' . PHP_EOL);
        fwrite($mysql_to_conf_file, 'host = ' . $dbTo->host . PHP_EOL);
        fwrite($mysql_to_conf_file, 'user = ' . $dbTo->username . PHP_EOL);
        fwrite($mysql_to_conf_file, 'password = ' . $dbTo->password . PHP_EOL);
        fclose($mysql_to_conf_file);

        // Create the command
        $mysqldump_from_command = 'mysqldump --defaults-extra-file="' . $mysqldump_from_conf_location . '" ' . $dbFrom->name . ' --no-create-db > ' . $mysqldump_from_location;

        // Execute the command
        $response = IO::shell($mysqldump_from_command);

        if (!empty($response)) {
            throw new DBDuplicationException('Error migrating ' . $databaseFrom . ' to ' . $databaseTo . ' [1/4]: ' . $response);
        }

        // Part 4: Clear TO database

        // Create the command (To create dump which drops tables)
        $mysqldump_to_command = 'mysqldump --defaults-extra-file="' . $mysql_to_conf_location . '" ' . $dbTo->name . ' --no-create-db --add-drop-table --no-data | grep "DROP" > ' . $mysqldump_to_location;

        // Execute the command
        $response = IO::shell($mysqldump_to_command);

        if (!empty($response)) {
            throw new DBDuplicationException('Error migrating ' . $databaseFrom . ' to ' . $databaseTo . ' [2/4]: ' . $response);
        }

        // Create the command (To drop tables based off previous dump)
        $mysql_to_command = 'mysql --defaults-extra-file=' . $mysql_to_conf_location . ' --connect_timeout=600 --max_allowed_packet=512M  ' . $dbTo->name . ' < ' . $mysqldump_to_location;

        // Execute the command
        $response = IO::shell($mysql_to_command);

        if (!empty($response)) {
            throw new DBDuplicationException('Error migrating ' . $databaseFrom . ' to ' . $databaseTo . ' [3/4]: ' . $response);
        }

        // Part 5: Upload TO database

        // Create the command
        $mysql_to_command = 'mysql --defaults-extra-file=' . $mysql_to_conf_location . ' --connect_timeout=600 --max_allowed_packet=512M  ' . $dbTo->name . ' < ' . $mysqldump_from_location;

        // Execute the command
        $response = IO::shell($mysql_to_command);

        if (!empty($response)) {
            throw new DBDuplicationException('Error migrating ' . $databaseFrom . ' to ' . $databaseTo . ' [4/4]: ' . $response);
        }

        // Delete the configuration and mysqldump files after we have finished using it
        unlink($mysqldump_from_conf_location);
        unlink($mysql_to_conf_location);
        unlink($mysqldump_to_location);
        unlink($mysqldump_from_location);

        // Close the customer DB connections
        $dbFrom->close();
        $dbTo->close();
    }

    private function processFileContent($directory, $efsFrom, $efsTo)
    {
        // Setup directory mapping
        $efsFromDirectory = $this->efsPrefix . $efsFrom . '/' . $directory . '/' . $this->efsFileDir;
        $efsToDirectory   = $this->efsPrefix . $efsTo . '/' . $directory . '/' . $this->efsFileDir;

        if (!file_exists($efsFromDirectory)) {
            throw new FileExistException('Directory {' . $efsFromDirectory . '} does not exist');
        }

        if (!file_exists($efsToDirectory)) {
            throw new FileExistException('Directory {' . $efsToDirectory . '} does not exist');
        }

        // Clean the TO directory if it already exists. Fix: (removed /*) as the directory needs to be removed before copy
        $response = IO::shell('rm -rf ' . $efsToDirectory . '/');

        if (!empty($response)) {
            throw new FileWriteException('Error clearing files from ' . strtoupper($efsTo) . ': ' . $response);
        }

        // Copy the contents of from to to directory
        $response = IO::shell('cp -auf ' . $efsFromDirectory . '/ ' . $efsToDirectory . '/');

        if (!empty($response)) {
            throw new FileWriteException('Error migrating files from ' . strtoupper($efsFrom) . ' to ' . strtoupper($efsTo) . ': ' . $response);
        }

        $this->changeOwnerMode($this->efsPrefix . $efsTo . '/' . $directory, 'www-data', 'www-data', '0775');
    }

    private function getCustomer($id)
    {
        $sql = "SELECT COUNT(1) AS `total` 
                FROM `customer` 
                WHERE `id` = :id";

        if (($stmt = $this->db->prepare($sql)) === false) {
            throw new PDOCreationException('PDOStatement prepare failed when fetching customer detail');
        }

        if ($stmt->execute(['id' => $id]) === false) {
            throw new PDOExecutionException('PDO query failed');
        }

        $row = $stmt->fetch();

        if ($row['total'] == 0) {
            throw new PDOResultEmptyException('The customer record with id {' . $id . '} does not exist');
        }

        $sql = "SELECT * 
                FROM `customer` 
                WHERE `id` = :id";

        if (($stmt = $this->db->prepare($sql)) === false) {
            throw new PDOCreationException('PDOStatement prepare failed when fetching customer detail');
        }

        if ($stmt->execute(['id' => $id]) === false) {
            throw new PDOExecutionException('PDOStatement execution failed when fetching customer detail');
        }

        return $stmt->fetch();
    }

    private function validateCustomerDB($customer, $environment)
    {
        if (!($validation = IO::required($customer, [$environment . '_username', $environment . '_password', $environment . '_hostname', $environment . '_database'], true))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $db = new Database([
            'DB_HOST'     => $customer[$environment . '_hostname'],
            'DB_PORT'     => 3306,
            'DB_NAME'     => $customer[$environment . '_database'],
            'DB_USERNAME' => $customer[$environment . '_username'],
            'DB_PASSWORD' => $customer[$environment . '_password']
        ]);

        if (!$db->test()) {
            throw new PDOExecutionException(strtoupper($environment) . ' database ' . $db->name . ' does not exist on server, cannot continue');
        }

        return $db;
    }

    private function sendEmail($config, $message)
    {
        if (!is_object($config)) {
            throw new MailException('Passed in parameter {config} must be an object');
        }

        $emailConfig = [
            'server'  => DEFAULT_MAIL_SERVER,
            'port'    => DEFAULT_MAIL_PORT,
            'from'    => DEFAULT_MAIL_FROM,
            'to'      => $config->name . ' <' . $config->email . '>',
            'subject' => 'CMS-Admin queue item processed [' . (defined('ENVIRONMENT') ? ENVIRONMENT : 'test') . ']'
        ];

        $mailer = new Mail($emailConfig);

        $message = 'Dear ' . $config->name . ',' . PHP_EOL . PHP_EOL . $message . PHP_EOL . PHP_EOL;
        $message .= 'Kind regards,' . PHP_EOL . PHP_EOL . PHP_EOL . 'CMS-Admin queue [' . (defined('ENVIRONMENT') ? ENVIRONMENT : 'test') . ']';

        $mailer->setMessage($message);
        $mailer->send();
    }

    private function changeOwnerMode($directory, $user, $group, $mode)
    {
        $retry = 0;
        
        while (!file_exists($directory)) {
            if ($retry++ > $this->efsRetry) {
                break;
            }
            sleep($this->efsDelay);
        }

        if (!file_exists($directory)) {
            throw new DirCreationException('Cannot detect directory {' . $directory . '}');
        }

        $response = IO::shell("chown -R {$user}:{$group} $directory");

        if (!empty($response)) {
            throw new FileWriteException('Failed to change ownership of directory {' . $directory . '}: ' . $response);
        }

        $response = IO::shell("chmod -R {$mode} $directory");

        if (!empty($response)) {
            throw new FileWriteException('Failed to change mode of directory {' . $directory . '}: ' . $response);
        }
    }
    #endregion
}
