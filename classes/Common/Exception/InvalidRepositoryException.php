<?php

namespace FS\Common\Exception;

class InvalidRepositoryException extends FSException
{
    public function __construct($message, $code = 1450, $type = 0)
    {
        parent::__construct($message, $code, $type);
    }
}
