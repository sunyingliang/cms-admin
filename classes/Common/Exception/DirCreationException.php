<?php

namespace FS\Common\Exception;

class DirCreationException extends FSException
{
    public function __construct($message, $code = 1350, $type = 0)
    {
        parent::__construct($message, $code, $type);
    }
}
