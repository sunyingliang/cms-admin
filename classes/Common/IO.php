<?php

namespace FS\Common;

use FS\Common\Exception\InvalidParameterException;

class IO
{
    public static function initResponseArray()
    {
        return [
            "status" => "success"
        ];
    }

    public static function formatResponseArray($responseArr)
    {
        return [
            "response" => $responseArr
        ];
    }

    public static function guid()
    {
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }

        $md5 = strtoupper(md5(uniqid(mt_srand(intval(microtime(true) * 1000)), true)));

        return substr($md5, 0, 8) . '-' . substr($md5, 8, 4) . '-' . substr($md5, 12, 4) . '-' . substr($md5, 16, 4) . '-' . substr($md5, 20, 12);
    }

    public static function generateRandom($length)
    {
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= chr(rand(33, 123));
        }

        return $string;
    }

    public static function generatePassword()
    {
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }

        return strtoupper(md5(uniqid(mt_srand(intval(microtime(true) * 1000)), true)));
    }

    public static function shell($cmd, $stdIn = null, &$stdOut = null, &$stdErr = null)
    {
        $descriptorSpec = [
            0 => ["pipe", "r"],
            1 => ["pipe", "w"],
            2 => ["pipe", "w"]
        ];

        $process = proc_open($cmd, $descriptorSpec, $pipes);

        if (!is_resource($process)) {
            throw new Exception('Error opening command process');
        }

        fwrite($pipes[0], $stdIn);
        fclose($pipes[0]);

        $stdOut = stream_get_contents($pipes[1]);
        $stdErr = stream_get_contents($pipes[2]);

        proc_close($process);

        return $stdOut . $stdErr;
    }

    public static function getQueryParameters()
    {
        return $_GET;
    }

    public static function getPostParameters()
    {
        $contentType = '';

        if (isset($_SERVER['CONTENT_TYPE'])) {
            $contentType = strtolower($_SERVER['CONTENT_TYPE']);
        }

        if (strpos($contentType, 'json') !== false) {
            $input = json_decode(file_get_contents('php://input'), true);

            if (json_last_error() !== JSON_ERROR_NONE) {
                $input = null;
            }
        } else {
            $input = $_POST;
        }

        return $input;
    }

    public static function default($haystack, $needle, $default = null)
    {
        if (is_numeric($default)) {
            return (isset($haystack[$needle]) && is_numeric($haystack[$needle])) ? intval($haystack[$needle]) : $default;
        } else {
            return (isset($haystack[$needle]) && !empty($haystack[$needle])) ? $haystack[$needle] : $default;
        }
    }

    public static function required($data, $mandatory = null, $strict = false)
    {
        $retValue = [
            'valid'   => false,
            'message' => ''
        ];

        if (is_string($data)) {
            if (defined($data)) {
                $data = constant($data);
            } else {
                $retValue['message'] = 'The given data does not exist';
                return $retValue;
            }
        }

        $required = [];

        if (is_object($data)) {
            $data = (array) $data;
        }

        if (!isset($data) || !is_array($data)) {
            $retValue['message'] = 'The given data must be a valid array';
        }

        foreach ($mandatory as $item) {
            if (!isset($data[$item])) {
                $required[] = $item;
            } else if ($strict && empty($data[$item])) {
                $required[] = $item;
            }
        }

        if (empty($required)) {
            return ['valid' => true];
        } else {
            $retValue['message'] = 'Required parameters: ' . implode(', ', $required);
        }

        return $retValue;
    }

    public static function getPDOConnection($connection)
    {
        if (!is_array($connection) || !isset($connection['dns']) || !isset($connection['username']) || !isset($connection['password'])) {
            throw new InvalidParameterException('The PDO connection information is not wrapped correctly');
        }

        $pdo = new \PDO($connection['dns'], $connection['username'], $connection['password']);
        $pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        $pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        return $pdo;
    }

    public static function doMessage($message, $slack = false) {
        if (!empty($message)) {
            if ($slack && defined('SLACK_URL') && !empty(SLACK_URL)
                && defined('SLACK_CHANNEL') && !empty(SLACK_CHANNEL) 
                && defined('SLACK_USERNAME') && !empty(SLACK_USERNAME)) {

                // Define Slack data
                $data = '{"channel": "' . SLACK_CHANNEL . '", "username": "' . SLACK_USERNAME . '", "text": "' . '<!channel> ' . $message . '"}';

                // Post to Slack server
                $ch = curl_init(SLACK_URL);
                
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_exec($ch);
                curl_close($ch);
            }

            $messageFull = date('H:i:s') . ' -> ' . $message . '...' . PHP_EOL;

            echo $messageFull;

            // Audit log (Limit 1MB)
            $file        = __DIR__ . '/../../' . date("Ymd") . '.log';
            $fileContent = '';

            if (file_exists($file)) {
                $fileContent = file_get_contents($file);
            } else {
                fclose(fopen($file, "wb"));
            }

            if (is_writable($file)) {
                $fileContent = date('Y/m/d') . ' ' . $messageFull . $fileContent;
                $fileContent = substr($fileContent, 0, 1048576);
                file_put_contents($file, $fileContent);
            }

            sleep(1);
        }
    }
}
