<?php

namespace FS\Common;

use FS\Common\Exception\FileNotReadableException;
use FS\Common\Exception\FileReadException;

class Auth
{
    private $tokenCachePath;
    private $token;

    public function __construct()
    {
        $this->tokenCachePath = TOKEN_CACHED_FILE;

        // Initialise token
        $get  = IO::getQueryParameters();
        $post = IO::getPostParameters();

        $token = isset($get['token']) ? $get['token'] : '';

        if (empty($token) && isset($post['token']) && !empty($post['token'])) {
            $token = $post['token'];
        }

        $this->token = $token;
    }

    #region Methods
    public function hasPermission($securable) {

        if (!isset($this->token) || empty($this->token) || ($permissions = $this->authenticate()) === false) {
            http_response_code(401);
            exit();
        }

        foreach ($securable as $permission) {
            if ($permissions[$permission] != 1) {
                http_response_code(403);
                exit();
            }
        }

        return true;
    }

    private function authenticate()
    {
        $counter = 0;
        $limit   = 2;

        while (true) {
            if (!is_readable($this->tokenCachePath)) {
                if ($counter < $limit) {
                    $counter++;
                    sleep(1);
                    continue;
                } else {
                    throw new FileNotReadableException('Token cached file {' . $this->tokenCachePath . '} is not readable');
                }
            }

            break;
        }

        $permission = [];
        $tokenStr   = file_get_contents($this->tokenCachePath);

        if ($tokenStr === false) {
            throw new FileReadException('Retrieve cached tokens failed');
        }

        $tokenArr = explode(';', $tokenStr);

        foreach ($tokenArr as $token) {
            if (strpos($token, $this->token) !== false) {
                $itemArr = explode('|', $token);

                if ($itemArr[1] == $this->token) {
                    $permission['read']  = $itemArr[2];
                    $permission['write'] = $itemArr[3];
                }

                break;
            }
        }

        return empty($permission) ? false : $permission;
    }
    #endregion
}
