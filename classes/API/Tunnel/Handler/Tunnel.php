<?php

namespace FS\API\Tunnel\Handler;

use FS\API\CMSAdminBase;
use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\IO;

class Tunnel extends CMSAdminBase
{
    public function __construct()
    {
        parent::__construct(['read']);
    }

    #region Methods
    public function getDomainByIP()
    {
        if (!($validation = IO::required($this->data, ['ip']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        # Note: Purposely does not check for enabled here as tunnel instances always need an ip address if they're active
        $sql = 'SELECT `live_tunnel_endpoint` FROM `customer` WHERE `live_tunnel_ip` = :live_tunnel_ip LIMIT 1';

        if (($stmt = $this->db->prepare($sql)) === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute(['live_tunnel_ip' => $this->data['ip']]) === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        $row = $stmt->fetch();

        if ($row) {
            $this->responseArr['data'] = [
                'endpoint' => $row['live_tunnel_endpoint']
            ];
        } else {
            throw new PDOExecutionException("Could not find endpoint for " . $this->data['ip']);
        }

        return $this->responseArr;
    }
    #endregion
}
