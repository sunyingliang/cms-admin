<?php

namespace FS\API;

use FS\Common\Exception\InvalidParameterException;
use FS\Common\IO;

class CMSCustomerRecordBase
{
    #region Properties
    private $id;
    private $name;

    private $devUsername;
    private $devPassword;
    private $devHostname;
    private $devDatabase;
    private $devBranch;

    private $stagingUsername;
    private $stagingPassword;
    private $stagingHostname;
    private $stagingDatabase;
    private $stagingBranch;

    private $liveUsername;
    private $livePassword;
    private $liveHostname;
    private $liveDatabase;
    private $liveBranch;
    private $liveDomain;
    private $liveAliases;
    private $liveTunnelEndpoint;
    private $liveTunnelIp;

    private $repository;
    private $directory;
    private $backupFrequency;
    private $enabled;

    #endregion

    public function __construct(array $data)
    {
        if (!is_array($data)) {
            throw new InvalidParameterException('Passed in parameters must be array format');
        }

        $this->id                 = 0;
        $this->name               = '';
        $this->devUsername        = '';
        $this->devPassword        = '';
        $this->devHostname        = '';
        $this->devDatabase        = '';
        $this->devBranch          = '';
        $this->devAliases         = '';
        $this->stagingUsername    = '';
        $this->stagingPassword    = '';
        $this->stagingHostname    = '';
        $this->stagingDatabase    = '';
        $this->stagingBranch      = '';
        $this->liveUsername       = '';
        $this->livePassword       = '';
        $this->liveHostname       = '';
        $this->liveDatabase       = '';
        $this->liveBranch         = '';
        $this->liveDomain         = '';
        $this->liveAliases        = '';
        $this->liveTunnelEndpoint = '';
        $this->liveTunnelIp       = '';
        $this->repository         = '';
        $this->directory          = '';
        $this->backupFrequency    = '';

        if (isset($data['id'])) {
            $this->setId($data['id']);
        }

        if (isset($data['name'])) {
            $this->setName($data['name']);
        }

        if (isset($data['devUsername'])) {
            $this->setDevUsername($data['devUsername']);
        } else if (isset($data['name'])) {
            $this->devUsername = $data['name'];
        }

        if (isset($data['devPassword'])) {
            $this->setDevPassword($data['devPassword']);
        } else {
            $this->setDevPassword(IO::generatePassword());
        }

        if (isset($data['devHostname'])) {
            $this->setDevHostname($data['devHostname']);
        } else {
            $this->setDevHostname(DB_HOSTNAME_DEV);
        }

        if (isset($data['devDatabase'])) {
            $this->setDevDatabase($data['devDatabase']);
        } else if (isset($data['name'])) {
            $this->setDevDatabase($data['name']);
        }

        if (isset($data['devBranch'])) {
            $this->setDevBranch($data['devBranch']);
        }

        if (isset($data['devAliases'])) {
            $this->setDevAliases($data['devAliases']);
        }

        if (isset($data['stagingUsername'])) {
            $this->setStagingUsername($data['stagingUsername']);
        } else if (isset($data['name'])) {
            $this->stagingUsername = $data['name'];
        }

        if (isset($data['stagingPassword'])) {
            $this->setStagingPassword($data['stagingPassword']);
        } else {
            $this->setStagingPassword(IO::generatePassword());
        }

        if (isset($data['stagingHostname'])) {
            $this->setStagingHostname($data['stagingHostname']);
        } else {
            $this->setStagingHostname(DB_HOSTNAME_STAGING);
        }

        if (isset($data['stagingDatabase'])) {
            $this->setStagingDatabase($data['stagingDatabase']);
        } else if (isset($data['name'])) {
            $this->setStagingDatabase($data['name']);
        }

        if (isset($data['stagingBranch'])) {
            $this->setStagingBranch($data['stagingBranch']);
        }

        if (isset($data['liveUsername'])) {
            $this->setLiveUsername($data['liveUsername']);
        } else if (isset($data['name'])) {
            $this->liveUsername = $data['name'];
        }

        if (isset($data['livePassword'])) {
            $this->setLivePassword($data['livePassword']);
        } else {
            $this->setLivePassword(IO::generatePassword());
        }

        if (isset($data['liveHostname'])) {
            $this->setLiveHostname($data['liveHostname']);
        } else {
            $this->setLiveHostname(DB_HOSTNAME_LIVE);
        }

        if (isset($data['liveDatabase'])) {
            $this->setLiveDatabase($data['liveDatabase']);
        } else if (isset($data['name'])) {
            $this->setLiveDatabase($data['name']);
        }

        if (isset($data['liveBranch'])) {
            $this->setLiveBranch($data['liveBranch']);
        }

        if (isset($data['liveDomain'])) {
            $this->setLiveDomain($data['liveDomain']);
        }

        if (isset($data['liveAliases'])) {
            $this->setLiveAliases($data['liveAliases']);
        }

        if (!empty($data['liveTunnelEndpoint'])) {
            $this->setLiveTunnelEndpoint($data['liveTunnelEndpoint']);
        }

        if (!empty($data['liveTunnelIp'])) {
            $this->setLiveTunnelIp($data['liveTunnelIp']);
        }

        if (isset($data['repository'])) {
            $this->setRepository($data['repository']);
        }

        if (isset($data['directory'])) {
            $this->setDirectory($data['directory']);
        }

        if (isset($data['backupFrequency'])) {
            $this->setBackupFrequency($data['backupFrequency']);
        }

        if (isset($data['enabled'])) {
            $this->setEnabled($data['enabled']);
        }
    }

    #region Getters & Setters
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getDevUsername()
    {
        return $this->devUsername;
    }

    public function setDevUsername($devUsername)
    {
        $this->devUsername = $devUsername;
    }

    public function getDevPassword()
    {
        return $this->devPassword;
    }

    public function setDevPassword($devPassword)
    {
        $this->devPassword = $devPassword;
    }

    public function getDevHostname()
    {
        return $this->devHostname;
    }

    public function setDevHostname($devHostname)
    {
        $this->devHostname = $devHostname;
    }

    public function getDevDatabase()
    {
        return $this->devDatabase;
    }

    public function setDevDatabase($devDatabase)
    {
        $this->devDatabase = $devDatabase;
    }

    public function getDevBranch()
    {
        return $this->devBranch;
    }

    public function setDevBranch($devBranch)
    {
        $this->devBranch = $devBranch;
    }

    public function getDevAliases()
    {
        return $this->devAliases;
    }

    public function setDevAliases($devAliases)
    {
        $this->devAliases = $devAliases;
    }

    public function getStagingUsername()
    {
        return $this->stagingUsername;
    }

    public function setStagingUsername($stagingUsername)
    {
        $this->stagingUsername = $stagingUsername;
    }

    public function getStagingPassword()
    {
        return $this->stagingPassword;
    }

    public function setStagingPassword($stagingPassword)
    {
        $this->stagingPassword = $stagingPassword;
    }

    public function getStagingHostname()
    {
        return $this->stagingHostname;
    }

    public function setStagingHostname($stagingHostname)
    {
        $this->stagingHostname = $stagingHostname;
    }

    public function getStagingDatabase()
    {
        return $this->stagingDatabase;
    }

    public function setStagingDatabase($stagingDatabase)
    {
        $this->stagingDatabase = $stagingDatabase;
    }

    public function getStagingBranch()
    {
        return $this->stagingBranch;
    }

    public function setStagingBranch($stagingBranch)
    {
        $this->stagingBranch = $stagingBranch;
    }

    public function getLiveUsername()
    {
        return $this->liveUsername;
    }

    public function setLiveUsername($liveUsername)
    {
        $this->liveUsername = $liveUsername;
    }

    public function getLivePassword()
    {
        return $this->livePassword;
    }

    public function setLivePassword($livePassword)
    {
        $this->livePassword = $livePassword;
    }

    public function getLiveHostname()
    {
        return $this->liveHostname;
    }

    public function setLiveHostname($liveHostname)
    {
        $this->liveHostname = $liveHostname;
    }

    public function getLiveDatabase()
    {
        return $this->liveDatabase;
    }

    public function setLiveDatabase($liveDatabase)
    {
        $this->liveDatabase = $liveDatabase;
    }

    public function getLiveBranch()
    {
        return $this->liveBranch;
    }

    public function setLiveBranch($liveBranch)
    {
        $this->liveBranch = $liveBranch;
    }

    public function getLiveDomain()
    {
        return $this->liveDomain;
    }

    public function setLiveDomain($liveDomain)
    {
        $this->liveDomain = $liveDomain;
    }

    public function getLiveAliases()
    {
        return $this->liveAliases;
    }

    public function setLiveAliases($liveAliases)
    {
        $this->liveAliases = $liveAliases;
    }

    public function getLiveTunnelEndpoint()
    {
        return $this->liveTunnelEndpoint;
    }

    public function setLiveTunnelEndpoint($liveTunnelEndpoint)
    {
        $this->liveTunnelEndpoint = $liveTunnelEndpoint;
    }

    public function getLiveTunnelIp()
    {
        return $this->liveTunnelIp;
    }

    public function setLiveTunnelIp($liveTunnelIp)
    {
        $this->liveTunnelIp = $liveTunnelIp;
    }

    public function getRepository()
    {
        return $this->repository;
    }

    public function setRepository($repository)
    {
        $this->repository = $repository;
    }

    public function getDirectory()
    {
        return $this->directory;
    }

    public function setDirectory($directory)
    {
        $this->directory = $directory;
    }

    public function getBackupFrequency()
    {
        return $this->backupFrequency;
    }

    public function setBackupFrequency($backupFrequency)
    {
        $this->backupFrequency = $backupFrequency;
    }

    public function getEnabled()
    {
        return $this->enabled;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    #endregion

    #region Utils
    private function checkIp($ip)
    {
        // Regular express check for ip format
        $regExp = '/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/';

        return preg_match($regExp, $ip) === 1;
    }

    public function validate()
    {
        if (!is_numeric($this->id)) {
            throw new InvalidParameterException('Passed in parameter {id} must be integer');
        }

        if (strlen($this->name) > 50) {
            throw new InvalidParameterException('Passed in parameter {name} exceeds maximum length (50)');
        }

        if ($this->checkSpace($this->name)) {
            throw new InvalidParameterException('Passed in parameter {name} should not contain spaces');
        }

        if (strlen($this->devUsername) > 64) {
            throw new InvalidParameterException('Passed in parameter {devUsername} exceeds maximum length (64)');
        }

        if (strlen($this->devPassword) < 8) {
            throw new InvalidParameterException('Passed in parameter {devPassword} entropy too low, minimum 8 characters, recommend 16+ characters');
        }

        if (strlen($this->devPassword) > 64) {
            throw new InvalidParameterException('Passed in parameter {devPassword} exceeds maximum length (64)');
        }

        if (strlen($this->devHostname) > 255) {
            throw new InvalidParameterException('Passed in parameter {devHostname} exceeds maximum length (255)');
        }

        if (strlen($this->devDatabase) > 64) {
            throw new InvalidParameterException('Passed in parameter {devDatabase} exceeds maximum length (64)');
        }

        if (strlen($this->devBranch) > 64) {
            throw new InvalidParameterException('Passed in parameter {devBranch} exceeds maximum length (64)');
        }

        if (strlen($this->devAliases) > 255) {
            throw new InvalidParameterException('Passed in parameter {devAliases} exceeds maximum length (255)');
        }

        if ($this->checkSpace($this->devAliases)) {
            throw new InvalidParameterException('Passed in parameter {devAliases} should not contain spaces');
        }

        if (strlen($this->stagingUsername) > 64) {
            throw new InvalidParameterException('Passed in parameter {stagingUsername} exceeds maximum length (64)');
        }

        if (strlen($this->stagingPassword) < 8) {
            throw new InvalidParameterException('Passed in parameter {stagingPassword} entropy too low, minimum 8 characters, recommend 16+ characters');
        }

        if (strlen($this->stagingPassword) > 64) {
            throw new InvalidParameterException('Passed in parameter {stagingPassword} exceeds maximum length (64)');
        }
        if (strlen($this->stagingHostname) > 255) {
            throw new InvalidParameterException('Passed in parameter {stagingHostname} exceeds maximum length (255)');
        }
        if (strlen($this->stagingDatabase) > 64) {
            throw new InvalidParameterException('Passed in parameter {stagingDatabase} exceeds maximum length (64)');
        }
        if (strlen($this->stagingBranch) > 64) {
            throw new InvalidParameterException('Passed in parameter {stagingBranch} exceeds maximum length (64)');
        }

        if (strlen($this->liveUsername) > 64) {
            throw new InvalidParameterException('Passed in parameter {liveUsername} exceeds maximum length (64)');
        }

        if (strlen($this->livePassword) < 8) {
            throw new InvalidParameterException('Passed in parameter {livePassword} entropy too low, minimum 8 characters, recommend 16+ characters');
        }

        if (strlen($this->livePassword) > 64) {
            throw new InvalidParameterException('Passed in parameter {livePassword} exceeds maximum length (64)');
        }

        if (strlen($this->liveHostname) > 255) {
            throw new InvalidParameterException('Passed in parameter {liveHostname} exceeds maximum length (255)');
        }

        if (strlen($this->liveDatabase) > 64) {
            throw new InvalidParameterException('Passed in parameter {liveDatabase} exceeds maximum length (64)');
        }

        if (strlen($this->liveBranch) > 64) {
            throw new InvalidParameterException('Passed in parameter {liveBranch} exceeds maximum length (64)');
        }

        if (strlen($this->liveDomain) > 64) {
            throw new InvalidParameterException('Passed in parameter {liveDomain} exceeds maximum length (64)');
        }

        if (strlen($this->liveAliases) > 255) {
            throw new InvalidParameterException('Passed in parameter {liveAliases} exceeds maximum length (255)');
        }

        if ($this->checkSpace($this->liveAliases)) {
            throw new InvalidParameterException('Passed in parameter {liveAliases} should not contain spaces');
        }

        if (strlen($this->liveTunnelEndpoint) > 255) {
            throw new InvalidParameterException('Passed in parameter {liveTunnelEndpoint} exceeds maximum length (255)');
        }

        if (strlen($this->liveTunnelIp) > 15) {
            throw new InvalidParameterException('Passed in parameter {liveTunnelIp} exceeds maximum length (15)');
        }

        if (!empty($this->liveTunnelIp) && !$this->checkIp($this->liveTunnelIp)) {
            throw new InvalidParameterException('Passed in parameter {liveTunnelIp} is not in correct format');
        }

        if (strlen($this->repository) > 255) {
            throw new InvalidParameterException('Passed in parameter {repository} exceeds maximum length (255)');
        }

        if (strlen($this->directory) > 64) {
            throw new InvalidParameterException('Passed in parameter {directory} exceeds maximum length (64)');
        }
    }

    private function checkSpace($value)
    {
        return preg_match('/\s/', $value);
    }
    #endregion
}
