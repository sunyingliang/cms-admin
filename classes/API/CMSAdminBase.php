<?php

namespace FS\API;

use FS\Common\IO;
use FS\Common\Auth;
use FS\Common\Exception\InvalidParameterException;

class CMSAdminBase
{
    protected $db;
    protected $responseArr;
    protected $timeZone;
    protected $auth;
    protected $data;

    public function __construct($securable = null)
    {
        $this->responseArr = IO::initResponseArray();
        $this->auth        = new Auth();
        $this->timeZone    = date_default_timezone_get();

        // VALIDATION
        if (empty($this->timeZone)) {
            $this->timeZone = 'NZ';
        }      

        // Used only if an entire handler has the same permissions, otherwise it's done within the handler
        if (!is_null($securable)) {
            if (!is_array($securable)) {
                throw new InvalidParameterException('Error: Securable should be array');
            }

            $this->auth->hasPermission($securable);
        }

        $this->db   = $this->getDb();
        $this->data = IO::getPostParameters();
    }

    #region Methods
    protected function getDb()
    {
        try {
            $connection = new \PDO('mysql:host=' . DB_HOST . ';port=' . DB_PORT . ';dbname=' . DB_NAME, DB_USERNAME, DB_PASSWORD);
            $connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $connection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            $connection = null;
        }

        return $connection;
    }
    
    protected function appendSearch($searchString, $query)
    {
        return $searchString . (strpos($searchString, 'WHERE') == false ? ' WHERE ' :  ' AND ') . $query;
    }

    protected function bindParams(\PDOStatement $stmt, $parameters)
    {
        foreach ($parameters as $parameter => $value) {
            if (isset($parameter)){
                $stmt->bindValue($parameter, $value);
            }

        }

        return $stmt;
    }
    #endregion
}
