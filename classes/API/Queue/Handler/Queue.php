<?php

namespace FS\API\Queue\Handler;

use FS\API\CMSAdminBase;
use FS\Common\Exception\FileNotReadableException;
use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\MailException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\Exception\PDOResultEmptyException;
use FS\Common\Exception\DBDuplicationException;
use FS\Common\Exception\FileWriteException;
use FS\Common\Database;
use FS\Common\IO;
use FS\Common\Mail;

class Queue extends CMSAdminBase
{
    public function __construct()
    {
        parent::__construct(['write']);
    }

    #region Methods
    public function create()
    {
        if (!($validation = IO::required($this->data, ['function']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        // Remove token field to make data a pure data object
        unset($this->data['token']);

        // Update status of the customer
        $sql = "UPDATE `customer` SET `processing` = 1 
                WHERE `id` = :id 
                    AND `enabled` = 1";

        $stmt = $this->db->prepare($sql);

        if (!$stmt->execute(['id' => $this->data['id']]) && !$this->db->errorCode()) {
            throw new PDOExecutionException('Failed to update processing field when event is added to queue');
        }

        $this->data = json_encode($this->data);

        $sql = "INSERT INTO `queue` (`data`) VALUES (:data)";

        if (($stmt = $this->db->prepare($sql)) === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute(['data' => $this->data]) === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        $this->responseArr['data'] = [
            'message' => "Job successfully added to queue",
            'id'      => $this->db->lastInsertId()
        ];

        return $this->responseArr;
    }
    #endregion
}
