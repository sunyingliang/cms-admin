<?php

namespace FS\API\CMSMaster\Handler;

use FS\API\CMSAdminBase;
use FS\API\CMSMaster\Entity\CustomerRecord;
use FS\Common\Exception\DBDuplicationException;
use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOCreationException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\Exception\PDOQueryException;
use FS\Common\Exception\PDOResultEmptyException;
use FS\Common\IO;

class Customer extends CMSAdminBase
{
    public function __construct()
    {
        parent::__construct();
    }

    #region Methods
    public function create()
    {
        $this->auth->hasPermission(['write']);

        if (!($validation = IO::required($this->data, ['name', 'liveDomain', 'directory', 'devBranch', 'stagingBranch', 'liveBranch']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $customer = new CustomerRecord($this->data);

        $customer->validate();

        $sql = "SELECT COUNT(1) AS `total` FROM `customer` WHERE `name` = :name";

        $stmt = $this->db->prepare($sql);

        if ($stmt->execute(['name' => $customer->getName()]) === false) {
            throw new PDOQueryException('PDO query failed');
        }

        $row = $stmt->fetch();

        if ($row['total'] > 0) {
            throw new DBDuplicationException('Name {' . $customer->getName() . '} has been used');
        }

        $columns = '`name`, `dev_password`, `dev_branch`, `dev_aliases`, `staging_password`, `staging_branch`, `live_password`, `live_branch`, `live_domain`, `live_aliases`, `live_tunnel_endpoint`, `live_tunnel_ip`, `repository`, `directory`, `backup_frequency`';
        $params  = ':name, :dev_password, :dev_branch, :dev_aliases, :staging_password, :staging_branch, :live_password, :live_branch, :live_domain, :live_aliases, :live_tunnel_endpoint, :live_tunnel_ip, :repository, :directory, :backup_frequency';
        $values  = [
            'name'                 => $customer->getName(),
            'dev_password'         => $customer->getDevPassword(),
            'dev_branch'           => $customer->getDevBranch(),
            'dev_aliases'          => $customer->getDevAliases(),
            'staging_password'     => $customer->getStagingPassword(),
            'staging_branch'       => $customer->getStagingBranch(),
            'live_password'        => $customer->getLivePassword(),
            'live_branch'          => $customer->getLiveBranch(),
            'live_domain'          => $customer->getLiveDomain(),
            'live_aliases'         => $customer->getLiveAliases(),
            'live_tunnel_endpoint' => $customer->getLiveTunnelEndpoint(),
            'live_tunnel_ip'       => $customer->getLiveTunnelIp(),
            'repository'           => $customer->getRepository(),
            'directory'            => $customer->getDirectory(),
            'backup_frequency'     => $customer->getBackupFrequency()
        ];

        $sql = 'INSERT INTO `customer` (' . $columns . ') VALUES (' . $params . ')';

        if (($stmt = $this->db->prepare($sql)) === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute($values) === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        $this->responseArr['data'] = ['id' => $this->db->lastInsertId()];

        return $this->responseArr;
    }

    public function getById($id)
    {
        $this->auth->hasPermission(['read']);

        if (!($validation = IO::required($this->data, ['name', 'liveDomain', 'directory', 'devBranch', 'stagingBranch', 'liveBranch']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        if (!isset($id)) {
            throw new InvalidParameterException('Parameter {id} is missing');
        }

        $sql = 'SELECT * FROM `customer` WHERE `id` = :id';

        if (($stmt = $this->db->prepare($sql)) === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute(['id' => $id]) === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        if ($row = $stmt->fetch()) {
            $this->responseArr['data'] = [
                'id'                 => $row['id'],
                'name'               => $row['name'],
                'devBranch'          => $row['dev_branch'],
                'stagingBranch'      => $row['staging_branch'],
                'liveBranch'         => $row['live_branch'],
                'liveDomain'         => $row['live_domain'],
                'liveAliases'        => $row['live_aliases'],
                'liveTunnelEndpoint' => $row['live_tunnel_endpoint'],
                'liveTunnelIp'       => $row['live_tunnel_ip'],
                'directory'          => $row['directory'],
                'repository'         => $row['repository'],
                'backupFrequency'    => $row['backup_frequency'],
                'enabled'            => $row['enabled']
            ];
        } else {
            $this->responseArr['message'] = 'No record is returned based on id {' . $id . '}';
        }

        return $this->responseArr;
    }

    public function update()
    {
        $this->auth->hasPermission(['write']);

        if (!($validation = IO::required($this->data, ['id']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $customer = new CustomerRecord($this->data);
        $customer->validate();

        $sql = 'SELECT COUNT(1) AS `total` FROM `customer` WHERE `id` = :id';

        $stmt = $this->db->prepare($sql);

        if ($stmt->execute(['id' => $customer->getId()]) === false) {
            throw new PDOExecutionException('PDO query failed');
        }

        $row = $stmt->fetch();

        if ($row['total'] == 0) {
            throw new PDOResultEmptyException('The record with id {' . $this->data['id'] . '} does not exist');
        }

        if (isset($this->data['name'])) {
            $sql = "SELECT COUNT(1) AS `total` FROM `customer` WHERE `id` != " . $customer->getId() . " AND `name` = '" . $customer->getName() . "'";

            if (($stmt = $this->db->query($sql)) === false) {
                throw new PDOQueryException('PDO query failed');
            }

            $row = $stmt->fetch();

            if ($row['total'] > 0) {
                throw new DBDuplicationException('Name {' . $customer->getName() . '} has been used');
            }
        }

        $optional = ['name', 'liveTunnelEndpoint', 'liveTunnelIp', 'liveDomain', 'liveAliases', 'directory', 'repository', 'devBranch', 'devAliases', 'stagingBranch', 'liveBranch', 'backupFrequency', 'enabled',
            'devUsername', 'devHostname', 'devDatabase', 'stagingUsername', 'stagingHostname', 'stagingDatabase', 'liveUsername', 'liveHostname', 'liveDatabase'];

        $optionalMapping = [
            'name'               => 'name',
            'liveTunnelEndpoint' => 'live_tunnel_endpoint',
            'liveTunnelIp'       => 'live_tunnel_ip',
            'liveDomain'         => 'live_domain',
            'liveAliases'        => 'live_aliases',
            'directory'          => 'directory',
            'repository'         => 'repository',
            'devBranch'          => 'dev_branch',
            'devAliases'         => 'dev_aliases',
            'stagingBranch'      => 'staging_branch',
            'liveBranch'         => 'live_branch',
            'backupFrequency'    => 'backup_frequency',
            'enabled'            => 'enabled',
            'devUsername'        => 'dev_username',
            'devHostname'        => 'dev_hostname',
            'devDatabase'        => 'dev_database',
            'stagingUsername'    => 'staging_username',
            'stagingHostname'    => 'staging_hostname',
            'stagingDatabase'    => 'staging_database',
            'liveUsername'       => 'live_username',
            'liveHostname'       => 'live_hostname',
            'liveDatabase'       => 'live_database'
        ];

        $columns = '';
        $values  = [];

        if (!empty($customer->getDevPassword())) {
            array_push($optional, 'devPassword');
            $optionalMapping['devPassword'] = 'dev_password';
        }

        if (!empty($customer->getStagingPassword())) {
            array_push($optional, 'stagingPassword');
            $optionalMapping['stagingPassword'] = 'staging_password';
        }

        if (!empty($customer->getLivePassword())) {
            array_push($optional, 'livePassword');
            $optionalMapping['livePassword'] = 'live_password';
        }

        foreach ($optional as $option) {
            if (isset($this->data[$option])) {
                $columns .= '`' . $optionalMapping[$option] . '` = :' . $optionalMapping[$option] . ', ';
                $values[$optionalMapping[$option]] = $this->data[$option];
            }
        }

        if (empty($columns) || empty($values)) {
            throw new InvalidParameterException('At least 1 field and value should be given for update action');
        }

        $columns      = substr($columns, 0, -2);
        $sql          = 'UPDATE `customer` SET ' . $columns . ' WHERE `id` = :id';
        $values['id'] = $customer->getId();

        if (($stmt = $this->db->prepare($sql)) === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute($values) === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        return $this->responseArr;
    }

    public function delete($id)
    {
        $this->auth->hasPermission(['write']);

        if (!isset($id)) {
            throw new InvalidParameterException('Parameter {id} is missing');
        }

        $customer = new CustomerRecord(['id' => $id]);

        $sql = 'SELECT COUNT(1) AS `total` FROM `customer` WHERE `id` = :id';

        $stmt = $this->db->prepare($sql);

        if ($stmt->execute(['id' => $customer->getId()]) === false) {
            throw new PDOExecutionException('PDO query failed');
        }

        $row = $stmt->fetch();

        if ($row['total'] == 0) {
            throw new PDOResultEmptyException('The customer record with id {' . $id . '} does not exist');
        }

        $sql = 'DELETE FROM `customer` WHERE `id` = :id';

        if (($stmt = $this->db->prepare($sql)) === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute(['id' => $id]) === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        return $this->responseArr;
    }

    public function getList()
    {
        $this->auth->hasPermission(['read']);

        $customer = new CustomerRecord($this->data);

        $searchString = '';
        $parameters   = [];
        
        if (!empty($customer->getName())) {
            $searchString       = $this->appendSearch($searchString, '`name` LIKE :name');
            $parameters['name'] = '%' . $customer->getName() . '%';
        }

        if (!empty($customer->getLiveTunnelIp())) {
            $searchString               = $this->appendSearch($searchString, '`live_tunnel_ip` LIKE :liveTunnelIp');
            $parameters['liveTunnelIp'] = '%' . $customer->getLiveTunnelIp() . '%';
        }

        if (!empty($customer->getLiveTunnelEndpoint())) {
            $searchString                     = $this->appendSearch($searchString, '`live_tunnel_endpoint` LIKE :liveTunnelEndpoint');
            $parameters['liveTunnelEndpoint'] = '%' . $customer->getLiveTunnelEndpoint() . '%';
        }

        if (!empty($customer->getLiveDomain())) {
            $searchString             = $this->appendSearch($searchString, '`live_domain` LIKE :liveDomain');
            $parameters['liveDomain'] = '%' . $customer->getLiveDomain() . '%';
        }

        if (!is_null($customer->getEnabled())) {
            $searchString                  = $this->appendSearch($searchString, 'enabled = :enabled');
            $parameters['enabled'] = $customer->getEnabled();
        }

        $sql = 'SELECT COUNT(1) AS `total` FROM `customer`' . $searchString;

        if (($stmt = $this->db->prepare($sql)) === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        if ($stmt->execute($parameters) === false) {
            throw new PDOCreationException('PDOStatement execution failed');
        }

        $this->responseArr['data'] = [];

        if ($row = $stmt->fetch()) {
            $this->responseArr['data']['totalRowCount'] = $row['total'];
        }

        $sql = 'SELECT * FROM `customer` ' . $searchString . ' ORDER BY `name`';

        if (isset($this->data['offset']) && isset($this->data['limit'])) {
            $sql .= ' LIMIT :offset, :limit';
        }

        if (($stmt = $this->db->prepare($sql)) === false) {
            throw new PDOCreationException('PDOStatement prepare failed');
        }

        $stmt = $this->bindParams($stmt, $parameters);

        if (isset($this->data['offset']) && isset($this->data['limit'])) {
            $stmt->bindValue('offset', intval($this->data['offset']), \PDO::PARAM_INT);
            $stmt->bindValue('limit', intval($this->data['limit']), \PDO::PARAM_INT);
        }

        if ($stmt->execute() === false) {
            throw new PDOCreationException('PDOStatement execution failed');
        }

        $this->responseArr['data']['rows'] = [];

        while ($row = $stmt->fetch()) {
            $this->responseArr['data']['rows'][] = [
                'id'                 => $row['id'],
                'name'               => $row['name'],
                'stage'              => $row['stage'],
                'processing'         => $row['processing'],
                'liveTunnelEndpoint' => $row['live_tunnel_endpoint'],
                'liveTunnelIp'       => $row['live_tunnel_ip'],
                'directory'          => $row['directory'],
                'liveDomain'         => $row['live_domain'],
                'liveAliases'        => $row['live_aliases'],
                'repository'         => $row['repository'],
                'devBranch'          => $row['dev_branch'],
                'devAliases'         => $row['dev_aliases'],
                'stagingBranch'      => $row['staging_branch'],
                'liveBranch'         => $row['live_branch'],
                'backupFrequency'    => $row['backup_frequency'],
                'enabled'            => $row['enabled'],
                'devUsername'        => $row['dev_username'],
                'devHostname'        => $row['dev_hostname'],
                'devDatabase'        => $row['dev_database'],
                'stagingUsername'    => $row['staging_username'],
                'stagingHostname'    => $row['staging_hostname'],
                'stagingDatabase'    => $row['staging_database'],
                'liveUsername'       => $row['live_username'],
                'liveHostname'       => $row['live_hostname'],
                'liveDatabase'       => $row['live_database'],
                'devPs'        => empty($row['dev_password']) ? false : true,
                'stagingPs'    => empty($row['staging_password']) ? false : true,
                'livePs'       => empty($row['live_password']) ? false : true,
            ];
        }

        return $this->responseArr;
    }
    #endregion
}
