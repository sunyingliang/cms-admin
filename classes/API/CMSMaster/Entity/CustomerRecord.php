<?php

namespace FS\API\CMSMaster\Entity;

use FS\API\CMSCustomerRecordBase;

# Child mirror class of shared CustomerRecord Entity
class CustomerRecord extends CMSCustomerRecordBase {}
