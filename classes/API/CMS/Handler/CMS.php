<?php

namespace FS\API\CMS\Handler;

use FS\API\CMSAdminBase;
use FS\API\CMS\Entity\VirtualHost;
use FS\API\CMS\Entity\CustomerRecord;
use FS\Common\Exception\InvalidParameterException;
use FS\Common\Exception\PDOExecutionException;
use FS\Common\Exception\PDOQueryException;
use FS\Common\Exception\PDOResultEmptyException;
use FS\Common\IO;

class CMS extends CMSAdminBase
{
    public function __construct()
    {
        parent::__construct(['read']);
    }

    #region Methods
    public function getApache2Config()
    {
        if (!($validation = IO::required($this->data, ['environment']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        // Change to lower in case some systems send different cases e.g. CMS-Automations
        $this->data['environment'] = strtolower($this->data['environment']);

        if (!in_array($this->data['environment'], ['dev', 'staging', 'live'])) {
            throw new InvalidParameterException('Parameter {environment} must be one of {dev, staging, live}');
        }

        $customer = new CustomerRecord($this->data);

        $sql = 'SELECT `name`,  
                       `directory`, 
                       `dev_hostname`, 
                       `dev_database`, 
                       `dev_username`, 
                       `dev_password`,
                       `dev_aliases`,
                       `staging_hostname`, 
                       `staging_database`, 
                       `staging_username`, 
                       `staging_password`, 
                       `live_hostname`, 
                       `live_database`, 
                       `live_username`, 
                       `live_password`,
                       `live_domain`, 
                       `live_aliases` 
                FROM `customer`';

        if ($this->data['environment'] == 'staging') {
            $whereClause = " WHERE (`stage` = 'STAGING' OR `stage` = 'LIVE') AND `enabled` = 1";
        } else if ($this->data['environment'] == 'live') {
            $whereClause = " WHERE `stage` = 'LIVE' AND `enabled` = 1";
        } else {
            $whereClause = " WHERE (`stage` = 'DEV' OR `stage` = 'STAGING' OR `stage` = 'LIVE') AND `enabled` = 1";
        }

        if (isset($this->data['name'])) {
            $whereClause .= ' AND `name` = :name';
        }

        if (!empty($whereClause)) {
            $sql .= $whereClause;
        }

        if (($stmt = $this->db->prepare($sql)) === false) {
            throw new PDOQueryException('PDOStatement prepare failed');
        }

        if (isset($this->data['name'])) {
            $stmt->bindValue('name', $customer->getName(), \PDO::PARAM_STR);
        }

        if ($stmt->execute() === false) {
            throw new PDOExecutionException('PDOStatement execution failed');
        }

        $output = '';

        if ($this->data['environment'] == 'live') {
            $rewriteDomains = [];
        }

        while ($row = $stmt->fetch()) {
            $output .= (new VirtualHost([
                    'environment'                 => $this->data['environment'],
                    'name'                        => $row['name'],
                    'devDomain'                   => DEV_DOMAIN,
                    'stagingDomain'               => STAGING_DOMAIN,
                    'liveDomain'                  => LIVE_DOMAIN,
                    'moveDomain'                  => MOVE_DOMAIN,
                    'devDomainAchieveService'     => DEV_DOMAIN_ACHIEVE_SERVICE,
                    'moveDomainAchieveService'    => MOVE_DOMAIN_ACHIEVESERVICE,
                    'stagingDomainAchieveService' => STAGING_DOMAIN_ACHIEVE_SERVICE,
                    'liveDomainAchieveService'    => LIVE_DOMAIN_ACHIEVE_SERVICE,
                    'directory'                   => $row['directory'],
                    'devHost'                     => $row['dev_hostname'],
                    'devDatabase'                 => $row['dev_database'],
                    'devUser'                     => $row['dev_username'],
                    'devPassword'                 => $row['dev_password'],
                    'devAliases'                  => $row['dev_aliases'],
                    'stagingHost'                 => $row['staging_hostname'],
                    'stagingDatabase'             => $row['staging_database'],
                    'stagingUser'                 => $row['staging_username'],
                    'stagingPassword'             => $row['staging_password'],
                    'liveHost'                    => $row['live_hostname'],
                    'liveDatabase'                => $row['live_database'],
                    'liveUser'                    => $row['live_username'],
                    'livePassword'                => $row['live_password'],
                    'dbLiveDomain'                => $row['live_domain'],
                    'liveAliases'                 => $row['live_aliases']
                ]))->generateOutput() . PHP_EOL . PHP_EOL;

            if (isset($rewriteDomains) && !empty($row['live_domain'])) {
                $rewriteDomains[] = strpos($row['live_domain'], 'www.') === false ? 'www.' . $row['live_domain'] : $row['live_domain'];
            }
        }

        $output = trim($output);

        if (isset($rewriteDomains) && !empty($rewriteDomains)) {
            $output = VirtualHost::prependRewriteRule($rewriteDomains) . PHP_EOL . PHP_EOL . trim($output);
        }

        $this->responseArr['data'] = $output;

        return $this->responseArr;
    }

    public function getBash()
    {
        if (!($validation = IO::required($this->data, ['environment']))['valid']) {
            throw new InvalidParameterException($validation['message']);
        }

        $efsPrefix        = '/efs/CMS-';
        $efsWebDir        = 'web';
        $efsFileDir       = 'files';
        $efsStagingPrefix = '/efs/CMS-staging';
        $webLocation      = '/var/www';
        $tab              = "\t";

        // Change to lower in case some systems send different cases e.g. CMS-Automations
        $this->data['environment'] = strtolower($this->data['environment']);

        if (!in_array($this->data['environment'], ['dev', 'staging', 'live'])) {
            throw new InvalidParameterException('Parameter {environment} must be one of {dev, staging, live}');
        }

        $sql = "SELECT `name`, `directory` FROM `customer`";

        if ($this->data['environment'] == 'staging') {
            $sql .= " WHERE (`stage` = 'STAGING' OR `stage` = 'LIVE') AND `enabled` = 1";
        } else if ($this->data['environment'] == 'live') {
            $sql .= " WHERE `stage` = 'LIVE' AND `enabled` = 1";
        } else {
            $sql .= " WHERE (`stage` = 'DEV' OR `stage` = 'STAGING' OR `stage` = 'LIVE') AND `enabled` = 1";
        }

        $result = $this->db->query($sql)->fetchAll(\PDO::FETCH_ASSOC);

        if (!$result) {
            throw new PDOResultEmptyException('No enabled customer record could be found for environment: ' . $this->data['environment']);
        }

        // Construct bash script
        $bash = '#!/bin/bash' . PHP_EOL;
        $bash .= 'declare -a customers' . PHP_EOL;
        $bash .= 'declare -a customersDirectory' . PHP_EOL;

        $resultCount = count($result);

        for ($i = 0; $i < $resultCount; $i++) {
            $bash .= 'customers[' . $i . ']=' . $result[$i]['name'] . PHP_EOL;
            $bash .= 'customersDirectory[' . $i . ']=' . (empty($result[$i]['directory']) ? $result[$i]['name'] : $result[$i]['directory']) . PHP_EOL;
        }

        $bash .= 'for ((i = 0; i < ${#customers[@]}; i++))' . PHP_EOL;
        $bash .= 'do' . PHP_EOL;

        if ($this->data['environment'] == 'dev' || $this->data['environment'] == 'staging') {
            $bash .= $tab . 'if [ -d "' . $efsPrefix . $this->data['environment'] . '/${customersDirectory[$i]}/' . $efsWebDir . '/sites/${customers[$i]}/files" ]; then' . PHP_EOL;
            $bash .= $tab . $tab . 'rm -rf ' . $efsPrefix . $this->data['environment'] . '/${customersDirectory[$i]}/' . $efsWebDir . '/sites/${customers[$i]}/files' . PHP_EOL;
            $bash .= $tab . 'elif [ -d "' . $efsPrefix . $this->data['environment'] . '/${customersDirectory[$i]}/' . $efsWebDir . '/sites/default/files" ]; then' . PHP_EOL;
            $bash .= $tab . $tab . 'rm -rf ' . $efsPrefix . $this->data['environment'] . '/${customersDirectory[$i]}/' . $efsWebDir . '/sites/default/files' . PHP_EOL;
            $bash .= $tab . 'fi' . PHP_EOL;
            $bash .= $tab . 'if [ ! -d "' . $webLocation . '/${customers[$i]}" ]; then' . PHP_EOL;
            $bash .= $tab . $tab . 'ln -s ' . $efsPrefix . $this->data['environment'] . '/${customersDirectory[$i]}/' . $efsWebDir . ' ' . $webLocation . '/${customers[$i]}' . PHP_EOL;
            $bash .= $tab . 'fi' . PHP_EOL;
        } else if ($this->data['environment'] == 'live') {
            $bash .= $tab . 'mkdir ' . $webLocation . '/${customers[$i]}' . PHP_EOL;
            $bash .= $tab . 'cp -a ' . $efsStagingPrefix . '/${customersDirectory[$i]}/' . $efsWebDir . '/. ' . $webLocation . '/${customers[$i]}' . PHP_EOL;
            $bash .= $tab . 'rm -rf ' . $webLocation . '/${customers[$i]}/.git' . PHP_EOL;
        }

        $bash .= $tab . 'if [ -d "' . $webLocation . '/${customers[$i]}/sites/${customers[$i]}" ]; then' . PHP_EOL;
        $bash .= $tab . $tab . 'rm -rf ' . $webLocation . '/${customers[$i]}/sites/${customers[$i]}/files' . PHP_EOL;
        $bash .= $tab . $tab . 'ln -s ' . $efsPrefix . $this->data['environment'] . '/${customersDirectory[$i]}/' . $efsFileDir . ' ' . $webLocation . '/${customers[$i]}/sites/${customers[$i]}/files' . PHP_EOL;
        $bash .= $tab . 'elif [ -d "' . $webLocation . '/${customers[$i]}/sites/default" ]; then' . PHP_EOL;
        $bash .= $tab . $tab . 'rm -rf ' . $webLocation . '/${customers[$i]}/sites/default/files' . PHP_EOL;
        $bash .= $tab . $tab . 'ln -s ' . $efsPrefix . $this->data['environment'] . '/${customersDirectory[$i]}/' . $efsFileDir . ' ' . $webLocation . '/${customers[$i]}/sites/default/files' . PHP_EOL;
        $bash .= $tab . 'fi' . PHP_EOL;
        $bash .= 'done';

        $this->responseArr['data'] = $bash;

        return $this->responseArr;
    }
    #endregion
}
