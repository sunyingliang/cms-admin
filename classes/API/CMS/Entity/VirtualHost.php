<?php

namespace FS\API\CMS\Entity;

use FS\Common\Exception\InvalidParameterException;

class VirtualHost
{
    public static $cT     = "\t";
    public static $cCRLF  = PHP_EOL;
    public static $cCRLFT = PHP_EOL . "\t";

    protected $environment;
    protected $customerName;
    protected $devDomain;
    protected $stagingDomain;
    protected $liveDomain;

    protected $moveDomain;

    protected $devDomainAchieveService;
    protected $stagingDomainAchieveService;
    protected $liveDomainAchieveService;
    protected $moveDomainAchieveService;

    protected $documentRoot;

    protected $devHost;
    protected $devDatabase;
    protected $devUser;
    protected $devPassword;
    protected $devAliases;
    protected $stagingHost;
    protected $stagingDatabase;
    protected $stagingUser;
    protected $stagingPassword;
    protected $liveHost;
    protected $liveDatabase;
    protected $liveUser;
    protected $livePassword;
    protected $dbLiveDomain;
    protected $liveAliases;

    public function __construct(array $data)
    {
        if (!is_array($data)) {
            throw new InvalidParameterException('Passed in parameters must be array format');
        }

        $this->environment   = strtolower($data['environment']);
        $this->customerName  = strtolower($data['name']);
        $this->devDomain     = strtolower($data['devDomain']);
        $this->stagingDomain = strtolower($data['stagingDomain']);
        $this->liveDomain    = strtolower($data['liveDomain']);
        $this->moveDomain    = strtolower($data['moveDomain']);

        $this->devDomainAchieveService     = strtolower($data['devDomainAchieveService']);
        $this->stagingDomainAchieveService = strtolower($data['stagingDomainAchieveService']);
        $this->liveDomainAchieveService    = strtolower($data['liveDomainAchieveService']);
        $this->moveDomainAchieveService    = strtolower($data['moveDomainAchieveService']);

        $this->documentRoot    = '/var/www/' . strtolower($data['name']);
        $this->devHost         = strtolower($data['devHost']);
        $this->devDatabase     = $data['devDatabase'];
        $this->devUser         = $data['devUser'];
        $this->devPassword     = $data['devPassword'];
        $this->devAliases      = empty($data['devAliases']) ? [] : explode(',', $data['devAliases']);
        $this->stagingHost     = strtolower($data['stagingHost']);
        $this->stagingDatabase = $data['stagingDatabase'];
        $this->stagingUser     = $data['stagingUser'];
        $this->stagingPassword = $data['stagingPassword'];
        $this->liveHost        = strtolower($data['liveHost']);
        $this->liveDatabase    = $data['liveDatabase'];
        $this->liveUser        = $data['liveUser'];
        $this->livePassword    = $data['livePassword'];
        $this->dbLiveDomain    = strtolower($data['dbLiveDomain']);
        $this->liveAliases     = empty($data['liveAliases']) ? [] : explode(',', $data['liveAliases']);
    }

    #region Methods
    public function generateOutput()
    {
        $result       = '<VirtualHost *:8080>';
        $envVariables = '';
        $serverName   = '';

        switch ($this->environment) {
            case 'dev':
                $serverName = $this->customerName . '.' . $this->devDomain;
                $result .= self::$cCRLFT . 'ServerName ' . $serverName;
                $result .= self::$cCRLFT . 'ServerAlias ' . $this->customerName . '.' . $this->devDomainAchieveService;
                $result .= self::$cCRLFT . 'ServerAlias ' . $this->customerName . '.' . $this->moveDomain;
                $result .= self::$cCRLFT . 'ServerAlias ' . $this->customerName . '.' . $this->moveDomainAchieveService;

                if (!empty($this->devAliases)) {
                    $result .= self::$cCRLFT . 'ServerAlias';
                    foreach ($this->devAliases as $alias) {
                        $result .= ' ' . trim($alias);
                    }
                }

                $envVariables .= 'SetEnv DATABASE_SERVER ' . $this->devHost;
                $envVariables .= self::$cCRLFT . 'SetEnv DATABASE_NAME ' . $this->devDatabase;
                $envVariables .= self::$cCRLFT . 'SetEnv DATABASE_USER ' . $this->devUser;
                $envVariables .= self::$cCRLFT . 'SetEnv DATABASE_PASSWORD ' . $this->devPassword;
                break;
            case 'staging':
                $serverName   = $this->customerName . '.' . $this->stagingDomain;
                $result       .= self::$cCRLFT . 'ServerName ' . $serverName;
                $result       .= self::$cCRLFT . 'ServerAlias ' . $this->customerName . '.' . $this->stagingDomainAchieveService;
                $envVariables .= 'SetEnv DATABASE_SERVER ' . $this->stagingHost;
                $envVariables .= self::$cCRLFT . 'SetEnv DATABASE_NAME ' . $this->stagingDatabase;
                $envVariables .= self::$cCRLFT . 'SetEnv DATABASE_USER ' . $this->stagingUser;
                $envVariables .= self::$cCRLFT . 'SetEnv DATABASE_PASSWORD ' . $this->stagingPassword;
                break;
            case 'live':
                $serverName = $this->customerName . '.' . str_replace('www.', '', $this->dbLiveDomain);
                $result     .= self::$cCRLFT . 'ServerName ' . $serverName;
                $result     .= self::$cCRLFT . 'ServerAlias ' . $this->customerName . '.' . $this->liveDomain;
                $result     .= self::$cCRLFT . 'ServerAlias ' . $this->customerName . '.' . $this->liveDomainAchieveService;

                foreach ($this->liveAliases as $alias) {
                    $result .= ' ' . trim($alias);
                }

                $envVariables .= 'SetEnv DATABASE_SERVER ' . $this->liveHost;
                $envVariables .= self::$cCRLFT . 'SetEnv DATABASE_NAME ' . $this->liveDatabase;
                $envVariables .= self::$cCRLFT . 'SetEnv DATABASE_USER ' . $this->liveUser;
                $envVariables .= self::$cCRLFT . 'SetEnv DATABASE_PASSWORD ' . $this->livePassword;
                break;
        }

        $result .= self::$cCRLFT . 'DocumentRoot ' . $this->documentRoot;
        $result .= self::$cCRLFT . '<Directory ' . $this->documentRoot . '/>';
        $result .= self::$cCRLFT . self::$cT . 'AllowOverride All';
        $result .= self::$cCRLFT . self::$cT . 'Require all granted';
        $result .= self::$cCRLFT . '</Directory>';
        $result .= self::$cCRLFT . $envVariables;
        $result .= self::$cCRLFT . 'ErrorLog /var/log/apache2/' . $serverName . '-error.log';
        $result .= self::$cCRLFT . 'CustomLog /var/log/apache2/' . $serverName . '-access.log combined';
        $result .= self::$cCRLFT . 'php_value newrelic.appname "All Hosts;CMS-' . ucfirst($this->environment) . ' ' . $this->customerName . ' [' . strtolower(ENVIRONMENT) . ']"';
        $result .= self::$cCRLFT . 'RewriteEngine on';
        $result .= self::$cCRLFT . 'RewriteRule "^/\.git/(.*)$" "/" [R]';
        $result .= self::$cCRLF . '</VirtualHost>';

        return $result;
    }

    public static function prependRewriteRule(array $rewriteDomains)
    {
        if (!is_array($rewriteDomains)) {
            throw new InvalidParameterException('Error: passed in parameter {rewriteDomain} must be a array');
        }

        $result = '<VirtualHost *:8080>';
        $result .= self::$cCRLFT . 'ServerAlias';

        foreach ($rewriteDomains as $alias) {
            $result .= ' ' . trim($alias);
        }

        $result .= self::$cCRLFT . 'RewriteEngine on';
        $result .= self::$cCRLFT . 'RewriteCond %{HTTP_HOST} ^www\.(.*)$ [NC]';
        $result .= self::$cCRLFT . 'RewriteRule ^ %{REQUEST_SCHEME}://%1%{REQUEST_URI} [R=301,L]';
        $result .= self::$cCRLF . '</VirtualHost>';

        return $result;
    }
    #endregion
}
