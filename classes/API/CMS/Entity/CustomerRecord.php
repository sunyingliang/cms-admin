<?php

namespace FS\API\CMS\Entity;

use FS\API\CMSCustomerRecordBase;

# Child mirror class of shared CustomerRecord Entity
class CustomerRecord extends CMSCustomerRecordBase {}
