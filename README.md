#FS-CMS-Admin


##Prerequisite
1. php version(>=7.0) is installed;

##Configuration  
1. deploy all these folders & files in web server;
2. configure 'public' folder as web server's document root, or change 'public' folder name to web server's document root;
3. rename all configuration files under /config folder to the name without '_example', e.g. change common_example.php to common.php.
4. run 'php /your-path-to-database.php/database.php' to create essential databases and tables;


##API Description
###API for tunnel server
1. Get back live domain name by live tunnel ip address
    * URI: /api/tunnel/domain
    * Method: POST
    * Params: token=yourToken&ip=ipParam
    * Return: 
    ~~~
    {
      "response": {
        "status": "success"
        "data": {
            "endpoint": "www.examplecustomer.gov.uk"
        }
      }
    }
    ~~~
  
###API for CMS server
1. Get apache2 configure content
    * URI: /api/cms/config
    * Method: POST
    * Params: token=yourToken&environment=environmentParam
    * Return: 
    ```
    <VirtualHost *:8080>
    	ServerName www.waltham-forest.gov.uk
    	ServerAlias test1.cms-live.firmsteptest.com alias_test1
    	DocumentRoot /var/www/firmstep_cms
    	AddHandler php5-script php
    	<Directory /var/www/firmstep_cms/>
    		AllowOverride All
    		Order allow,deny
    		allow from all
    	</Directory>
    	AccessFileName .htaccess
    	ErrorLog /var/log/apache2/error.log
    	CustomLog /var/log/apache2/access.log combined
    </VirtualHost>
    
    <VirtualHost *:8080>
    	ServerName www.waltham-forest.gov.uk
    	ServerAlias nameTest1.cms-live.firmsteptest.com waltham-forest.gov.uk
    	DocumentRoot /var/www/public
    	AddHandler php5-script php
    	<Directory /var/www/public/>
    		AllowOverride All
    		Order allow,deny
    		allow from all
    	</Directory>
    	AccessFileName .htaccess
    	ErrorLog /var/log/apache2/error.log
    	CustomLog /var/log/apache2/access.log combined
    </VirtualHost>
    ```
    
2. Get git paths
    * URI: /api/cms/rebuild
    * Method: POST
    * Params: token=yourToken&environment=environmentParam
    * Return: 
    ```
    #!/bin/bash
    
    declare -a customers
    customers[0]='firmstep-demo'
    
    rm -Rf /var/www/{.[^.]*,*}
    for i in "${customers[@]}"; do
    	mkdir /var/www/$i
    	cp -a /efs/CMS-staging/$i/web/. /var/www/$i
    	sitesDir=/var/www/$i/sites/
    	sitesDirLength=${#sitesDir}
    	for s in ${sitesDir}*; do
    		if [ -d "$s" ]; then
    		rm -f ${s}/files
    		ln -s /efs/CMS-live/$i/files/${s:$sitesDirLength} ${s}/files
    		fi
    	done
    done
    ```

###API for adminUI 
1. Add item into database
    * URI: /api/customer/add
    * Method: POST
    * Params: token=yourToken&name=nameParam&liveTunnelIp=liveTunnelIpParam&liveDomain=liveDomainParam&liveAliases=liveAliasesParam&
        directory=directoryParam&repository=repositoryParam&devBranch=devBranchParam&stagingBranch=stagingBranchParam&liveBranch=liveBranchParam[&devAliases=devAliasesParam]
    * Return:
    ```
    {
      "response": {
        "status": "success"
      }
    }
    ```  
              
2. list items from database
    * URI: /api/customer/list
    * Method: POST
    * Params: token=yourToken&offset=offsetParam&limit=limitParam
    * Return:
    ```
    {
      "response": {
        "status": "success",
        "data": {
          "totalRowCount": 5,
          "rows": [
            {
              "id": 1,
              "name": "test1",
              "liveTunnelIp": "127.0.0.1",
              "directory": "firmstep_cms",
              "liveDomain": "www.waltham-forest.gov.uk",
              "liveAliases": "alias_test1",
              "repository": "customer-test",
              "devBranch": "dev",
              "stagingBranch": "staging",
              "liveBranch": "live"
            }
          ]
        }
      }
    }
    ```    
               
3. Search items based on name from database
    * URI: /api/customer/search
    * Method: POST
    * Params: token=yourToken&name=nameParam&offset=offsetParam&limit=limitParam
    * Return:
    ```
    {
      "response": {
        "status": "success",
        "data": {
          "totalRowCount": 4,
          "rows": [
            {
              "id": 3,
              "name": "nameTest1",
              "liveTunnelIp": "192.168.1.3",
              "directory": "public",
              "liveDomain": "www.waltham-forest.gov.uk",
              "liveAliases": "waltham-forest.gov.uk",
              "repository": "testRepository",
              "devBranch": "testdev_branch",
              "stagingBranch": "teststaging_branch",
              "liveBranch": "testlive_branch"
            }
          ]
        }
      }
    }
    ``` 
    
4. Update item in database
    * URI: /api/customer/update
    * Method: POST
    * Params: token=yourToken&id=idParam&name=nameParam&liveTunnelIp=liveTunnelIpParam&liveDomain=liveDomainParam&liveAliases=liveAliasesParam&
        directory=directoryParam&repository=repositoryParam&devBranch=devBranchParam&stagingBranch=stagingBranchParam&liveBranch=liveBranchParam[&devAliases=devAliasesParam]
    * Return:
    ```
    {
      "response": {
        "status": "success"
      }
    }
    ``` 
 
5. Delete item from database
    * URI: /api/customer/delete/{id}
    * Method: GET
    * Params: token=yourToken
    * Return:
    ```
    {
      "response": {
        "status": "success"
      }
    }
    ```

6. Add job to queue
    * URI: /api/queue/add
    * Method: POST
    * Params: token=yourToken&function=functionValue&id=customerIdValue&name(optional)&email(optional)
    * Return:
    ```
    {
      "response": {
        "status": "success",
        "id": 123
      }
    }
    ```
    * Note: Optional parameters name and email will be used to send an email when the queued request is processed.


##Phpunit tests
###to run tests on linux
1. change directory to the folder containing all your content
2. make sure the web server is started
3. run the following command (NOTE: PHPUnit must be installed for this to work)
    * phpunit (assume phpunit is installed globally)
    * ./vendor/bin/phpunit (assume phpunit is not installed globally)

###to run on windows
1. change directory to the folder containing all your content
2. double click 'test-api.bat' to run phpunit tests
