<?php

class CustomerHandlerTest extends \PHPUnit_Framework_TestCase
{
    public $token;

    public function setup()
    {
        $this->token = 'A6BFF692-D891-4285-876C-38C963579ACC';
    }

    public function testCustomerHandler()
    {
        $randName = 'automated-unit-test-' . rand(90000,99999);

        //test add
        $data = 'token=' . $this->token
                . '&name=' . $randName
                . '&liveTunnelIp=127.0.0.1'
                . '&liveDomain=www.test.com'
                . '&liveAliases=test.com,test2.com'
                . '&directory=test_cms'
                . '&repository=git@github.com:test/test.git'
                . '&devBranch=NET-x'
                . '&stagingBranch=NET-x'
                . '&liveBranch=NET-x';

        $response = $this->runCurl('customer/create', [], $data);
        $decode   = json_decode($response['body']);
        $this->assertEquals(200, $response['status']);

        $id = $decode->response->data->id;

        //test list
        $data = 'token=' . $this->token
            . '&offset=0'
            . '&limit=10';
        $response = $this->runCurl('customer/list', [], $data);
        $this->assertEquals(200, $response['status']);

        //test get list by id
        $data = 'token=' . $this->token
            . '&name='
            . '&liveDomain='
            . '&directory='
            . '&devBranch='
            . '&stagingBranch='
            . '&liveBranch=';
        $response = $this->runCurl('customer/get/' . $id, [], $data);
        $this->assertEquals(200, $response['status']);
        $decode = json_decode($response['body']);
        $this->assertContains($randName, $decode->response->data->name);

        //test update
        $data = 'token=' . $this->token
                . '&id=' . $id
                . '&liveTunnelIp=127.100.100.100';
        $response = $this->runCurl('customer/update', [], $data);
        $this->assertEquals('200', $response['status']);

        //test delete
        $data = 'token=' . $this->token;
        $response = $this->runCurl('customer/delete/' . $id, [], $data);
        $this->assertEquals('200', $response['status']);
    }

    private function getServerURL()
    {
        return 'http://localhost:8080/api/';
    }

    private function runCurl(
        $url,
        $header = [],
        $post = null,
        $userPass = null,
        $override = false
    ) {
        if (!$override) {
            $url = $this->getServerURL() . $url;
        }

        $ch = curl_init($url);

        if (!empty($post)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (!empty($userPass)) {
            curl_setopt($ch, CURLOPT_USERPWD, $userPass);
        }

        $response = curl_exec($ch);
        $status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($status == 0) {
            $status = curl_error($ch);
        }

        curl_close($ch);

        return ['body' => $response, 'status' => $status];
    }
}
