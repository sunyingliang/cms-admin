<?php

class CMSHandlerTest extends PHPUnit_Framework_TestCase
{
    public $token;

    public function setup()
    {
        $this->token = 'A6BFF692-D891-4285-876C-38C963579ACC';
    }

    public function testGetConfig()
    {
        $data     = 'token=' . $this->token . '&environment=dev';
        $response = $this->runCurl('cms/config', [], $data);
        $this->assertEquals(200, $response['status']);
    }

    public function testGetBash()
    {
        $data     = 'token=' . $this->token . '&environment=dev';
        $response = $this->runCurl('cms/rebuild', [], $data);
        $this->assertEquals(200, $response['status']);
    }

    private function getServerURL()
    {
        return 'http://localhost:8080/api/';
    }

    private function runCurl(
        $url,
        $header = [],
        $post = null,
        $userPass = null,
        $override = false
    )
    {
        if (!$override) {
            $url = $this->getServerURL() . $url;
        }

        $ch = curl_init($url);

        if (!empty($post)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (!empty($userPass)) {
            curl_setopt($ch, CURLOPT_USERPWD, $userPass);
        }

        $response = curl_exec($ch);
        $status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($status == 0) {
            $status = curl_error($ch);
        }

        curl_close($ch);

        return ['body' => $response, 'status' => $status];
    }
}
